package database

import (
	"errors"
	"fmt"
	"gitlab.com/ylerby/slp/internal/domain"
	"gitlab.com/ylerby/slp/internal/model"
	"gorm.io/gorm"
)

func (d *Database) Create(record domain.CreateRecordFields) error {
	result := d.db.Exec(fmt.Sprintf(`
		insert into real_estates (
		price,
		geo_lat,
		geo_lon,
		region,
		building_type,
		level,
		levels,
		rooms,
		area,
		kitchen_area,
		object_type,
		city,
		osm_amenity_points_in_001,
		osm_building_points_in_001,
		osm_catering_points_in_001,
		osm_city_closest_dist,
		osm_city_nearest_name,
		osm_city_nearest_population,
		osm_crossing_closest_dist,
		osm_culture_points_in_001,
		osm_finance_points_in_001,
		osm_historic_points_in_001,
		osm_hotels_points_in_001,
		osm_leisure_points_in_001,
		osm_offices_points_in_001,
		osm_shops_points_in_001,
		osm_subway_closest_dist,
		osm_train_stop_closest_dist,
		osm_train_stop_points_in_001,
		osm_transport_stop_closest_dist,
		reform_count_of_houses_1000,
		reform_count_of_houses_500,
		many_offices,
		many_food,
		many_shops,
		many_financial_organizations,
		many_entertainment,
		many_historical_objects,
		many_hotels,
		station_rate,
		many_stations,
		many_culture_objects,
		many_comfort_objects,
		kremlin_dist,
		nearest_metro_station,
		metro_dist,
		walk_time_to_metro,
		apartment_type,
		street,
		house_number
		) values (
		          %f, %f, %f, '%s', '%s',
		          %d, %d, %d, %f, %f,
		          '%s', '%s', %d, %d, %d,
		          %f, '%s', %f, %f, %d,
		          %d, %d, %d, %d, %d,
		          %d, %f, %f, %d, %f,
		          %d, %d, %d, %d, %d,
		          %d, %d, %d, %d, %d,
		          %d, %d, %d, %f, '%s',
		          %f, %f, '%s', '%s', '%s'
		)`,
		record.Price,
		record.GeoLat,
		record.GeoLon,
		record.Region,
		record.BuildingType,
		record.Level,
		record.Levels,
		record.Rooms,
		record.Area,
		record.KitchenArea,
		record.ObjectType,
		record.City,
		record.OsmAmenityPointsIn0Dot01,
		record.OsmBuildingPointsIn0Dot01,
		record.OsmCateringPointIn0Dot01,
		record.OsmSubwayClosestDist,
		record.OsmCityNearestName,
		record.OsmCityNearestPopulation,
		record.OsmCrossingClosestDist,
		record.OsmCulturePointsIn0Dot01,
		record.OsmFinancePointsIn0Dot01,
		record.OsmHistoricPointsIn0Dot01,
		record.OsmHotelsPointsIn0Dot01,
		record.OsmLeisurePointsIn0Dot01,
		record.OsmOfficesPointsIn0Dot01,
		record.OsmShopsPointsIn0Dot01,
		record.OsmSubwayClosestDist,
		record.OsmTrainStopClosestDist,
		record.OsmTrainStopPointsIn0Dot01,
		record.OsmTransportStopClosestDist,
		record.ReformCountOfHouses1K,
		record.ReformCountOfHouses500,
		record.ManyOffices,
		record.ManyFood,
		record.ManyShops,
		record.ManyFinancialOrganizations,
		record.ManyEntertainment,
		record.ManyHistoricalObjects,
		record.ManyHotels,
		record.StationRate,
		record.ManyStations,
		record.ManyCultureObjects,
		record.ManyComfortObjects,
		record.KremlinDist,
		record.NearestMetroStation,
		record.MetroDist,
		record.WalkTimeToMetro,
		record.ApartmentType,
		record.Street,
		record.HouseNumber,
	))

	if err := result.Error; err != nil {
		return fmt.Errorf("db error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return fmt.Errorf("record not found")
	}

	return nil
}

func (d *Database) Get() ([]domain.RealEstate, error) {
	records := make([]domain.RealEstate, 0)

	result := d.db.Find(&records)

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("db error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return nil, fmt.Errorf("empty data")
	}

	return records, nil
}

func (d *Database) AdminLogin(login string) (*model.Admin, error) {
	record := new(model.Admin)
	result := d.db.Where("login = ?", login).Find(record)

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("db error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return nil, fmt.Errorf("record not found")
	}

	return record, nil
}

func (d *Database) Login(login string) (*model.Client, error) {
	record := new(model.Client)
	result := d.db.Where("login = ?", login).Find(record)

	if err := result.Error; err != nil {
		return nil, fmt.Errorf("db error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return nil, fmt.Errorf("record not found")
	}

	return record, nil
}

func (d *Database) Register(login, password string) error {
	record := &model.Client{
		Login:    login,
		Password: password,
	}

	result := d.db.Create(record)

	err := result.Error

	switch {
	case errors.Is(err, gorm.ErrDuplicatedKey):
		return fmt.Errorf("user already exists")
	case err != nil:
		return fmt.Errorf("db error occurred: %w", err)
	}

	return nil
}

func (d *Database) UpdateRecord(record domain.UpdateRecordFields) error {
	result := d.db.Exec(fmt.Sprintf(`
		update real_estates
		set
		price = %f,
		geo_lat = %f,
		geo_lon = %f,
		region = '%s',
		building_type = '%s',
		level = %d,
		levels = %d,
		rooms = %d,
		area = %f,
		kitchen_area = %f,
		object_type = '%s',
		city = '%s',
		osm_amenity_points_in_001 = %d,
		osm_building_points_in_001 = %d,
		osm_catering_points_in_001 = %d,
		osm_city_closest_dist = %f,
		osm_city_nearest_name = '%s',
		osm_city_nearest_population = %f,
		osm_crossing_closest_dist = %f,
		osm_culture_points_in_001 = %d,
		osm_finance_points_in_001 = %d,
		osm_historic_points_in_001 = %d,
		osm_hotels_points_in_001 = %d,
		osm_leisure_points_in_001 = %d,
		osm_offices_points_in_001 = %d,
		osm_shops_points_in_001 = %d,
		osm_subway_closest_dist = %f,
		osm_train_stop_closest_dist = %f,
		osm_train_stop_points_in_001 = %d,
		osm_transport_stop_closest_dist = %f,
		reform_count_of_houses_1000 = %d,
		reform_count_of_houses_500 = %d,
		many_offices = %d,
		many_food = %d,
		many_shops = %d,
		many_financial_organizations = %d,
		many_entertainment = %d,
		many_historical_objects = %d,
		many_hotels = %d,
		station_rate = %d,
		many_stations = %d,
		many_culture_objects = %d,
		many_comfort_objects = %d,
		kremlin_dist = %f,
		nearest_metro_station = '%s',
		metro_dist = %f,
		walk_time_to_metro = %f,
		apartment_type = '%s',
		street = '%s',
		house_number = '%s'
		where id = %d;`,

		record.Price,
		record.GeoLat,
		record.GeoLon,
		record.Region,
		record.BuildingType,
		record.Level,
		record.Levels,
		record.Rooms,
		record.Area,
		record.KitchenArea,
		record.ObjectType,
		record.City,
		record.OsmAmenityPointsIn0Dot01,
		record.OsmBuildingPointsIn0Dot01,
		record.OsmCateringPointIn0Dot01,
		record.OsmSubwayClosestDist,
		record.OsmCityNearestName,
		record.OsmCityNearestPopulation,
		record.OsmCrossingClosestDist,
		record.OsmCulturePointsIn0Dot01,
		record.OsmFinancePointsIn0Dot01,
		record.OsmHistoricPointsIn0Dot01,
		record.OsmHotelsPointsIn0Dot01,
		record.OsmLeisurePointsIn0Dot01,
		record.OsmOfficesPointsIn0Dot01,
		record.OsmShopsPointsIn0Dot01,
		record.OsmSubwayClosestDist,
		record.OsmTrainStopClosestDist,
		record.OsmTrainStopPointsIn0Dot01,
		record.OsmTransportStopClosestDist,
		record.ReformCountOfHouses1K,
		record.ReformCountOfHouses500,
		record.ManyOffices,
		record.ManyFood,
		record.ManyShops,
		record.ManyFinancialOrganizations,
		record.ManyEntertainment,
		record.ManyHistoricalObjects,
		record.ManyHotels,
		record.StationRate,
		record.ManyStations,
		record.ManyCultureObjects,
		record.ManyComfortObjects,
		record.KremlinDist,
		record.NearestMetroStation,
		record.MetroDist,
		record.WalkTimeToMetro,
		record.ApartmentType,
		record.Street,
		record.HouseNumber,
		record.ID,
	))

	if err := result.Error; err != nil {
		return fmt.Errorf("db error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return fmt.Errorf("record not found")
	}

	return nil
}

func (d *Database) DeleteRecord(id int) error {
	result := d.db.Exec(fmt.Sprintf(`delete from real_estates where id = %d;`, id))

	if err := result.Error; err != nil {
		return fmt.Errorf("db error occurred: %w", err)
	}

	if result.RowsAffected == 0 {
		return fmt.Errorf("record not found")
	}

	return nil
}

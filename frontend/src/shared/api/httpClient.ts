/* eslint-disable no-console */
import axios, { AxiosRequestConfig, AxiosResponse } from "axios";

import { stations } from "../constants/stations";

import { getToken } from "./getToken";

export interface UseLoginMutationParams {
  login: string;
  password: string;
}

export interface UseRegisterMutationParams extends UseLoginMutationParams {
  againPassword: string;
}

export enum Region {
  Moscow = "Москва",
  MoscowArea = "Московская область",
}
export enum ObjectType {
  New = "Новое строение",
  Second = "Вторичное",
}
export enum Building {
  Block = "Блочный",
  Other = "Другое",
  Brick = "Кирпичный",
  Monolit = "Монолитный",
  Panel = "Панельный",
  Wood = "Деревянный",
}

export enum ApartmentType {
  Apartment = "Квартира",
  Studio = "Студия",
  PentHouse = "Пент-хаус",
  Apartments = "Аппартаменты",
}

export interface AdType {
  id: number;
  price: number;
  "geo-lat": number;
  "geo-lon": number;
  region: Region;
  "building-type": Building;
  level: number;
  levels: number;
  rooms: number;
  area: number;
  "kitchen-area": number;
  "object-type": ObjectType;
  city: string;
  "osm-amenity-points-in-001": number;
  "osm-building-points-in-001": number;
  "osm-catering-point-in-001": number;
  "osm-city-closest-dist": number;
  "osm-city-nearest-name": string;
  "osm-city-nearest-population": number;
  "osm-crossing-closest-dist": number;
  "osm-culture-points-in-001": number;
  "osm-finance-points-in-001": number;
  "osm-historic-points-in-001": number;
  "osm-hotels-points-in-001": number;
  "osm-leisure-points-in-001": number;
  "osm-offices-points-in-001": number;
  "osm-shops-points-in-001": number;
  "osm-subway-closest-dist": number;
  "osm-train-stop-closest-dist": number;
  "osm-train-stop-points-in-001": number;
  "osm-transport-stop-closest-dist": number;
  "reform-count-of-houses-1k": number;
  "reform-count-of-houses-500": number;
  "many-offices": number;
  "many-food": number;
  "many-shops": number;
  "many-financial-organizations": number;
  "many-entertainment": number;
  "many-historical-objects": number;
  "many-hotels": number;
  "station-rate": number;
  "many-stations": number;
  "many-culture-objects": number;
  "many-comfort-objects": number;
  "kremlin-dist": number;
  "nearest-metro-station": (typeof stations)[number];
  "metro-dist": number;
  "walk-time-to-metro": number;
  "apartment-type": ApartmentType;
  street: string;
  "house-number": string;
}

export interface GetPredictionPayload {
  "apartment-type": ApartmentType;
  street: string;
  "house-number": string;
  region: Region;
  "building-type": Building;
  level: number;
  levels: number;
  rooms: number;
  area: number;
  "kitchen-area": number;
  "object-type": ObjectType;
  city: string;
}

export class HttpClient {
  static backendUrl = process.env.REACT_APP_BACKEND_URL;

  static getHeadersWithToken() {
    const token = getToken();
    const headers: AxiosRequestConfig["headers"] = {};

    if (token) {
      headers["Authorization"] = `Bearer ${token}`;
    }

    return headers;
  }

  static register(params: UseRegisterMutationParams) {
    const { againPassword, ...otherParams } = params;

    return axios({
      method: "post",
      url: "/api/v1/register",
      data: { ...otherParams, "again-password": againPassword },
    });
  }

  static loginUser(params: UseLoginMutationParams) {
    return axios({
      method: "post",
      url: "/api/v1/login/user",
      data: params,
    });
  }

  static loginAdmin(params: UseLoginMutationParams) {
    return axios({
      method: "post",
      url: "/api/v1/login/admin",
      data: params,
    });
  }

  static getAds(): Promise<AxiosResponse<AdType[]>> {
    return axios({
      method: "get",
      url: "/api/v1/get",
    });
  }

  static deleteAd(id: number): Promise<AxiosResponse<AdType[]>> {
    return axios({
      method: "delete",
      url: "/api/v1/delete",
      headers: this.getHeadersWithToken(),
      data: {
        id,
      },
    });
  }

  static createAd(params: any): Promise<AxiosResponse<AdType[]>> {
    return axios({
      method: "post",
      url: "/api/v1/create",
      headers: this.getHeadersWithToken(),
      data: params,
    });
  }

  static getPrediction(
    payload: GetPredictionPayload
  ): Promise<AxiosResponse<AdType[]>> {
    return axios({
      method: "post",
      url: "https://h3ct0rzzz-real-estate-valuation-mlops-931f.twc1.net:443/predict",
      data: payload,
    });
  }

  static updateAd(payload: AdType): Promise<AxiosResponse<AdType[]>> {
    return axios({
      method: "put",
      url: "/api/v1/update",
      headers: this.getHeadersWithToken(),
      data: payload,
    });
  }
}

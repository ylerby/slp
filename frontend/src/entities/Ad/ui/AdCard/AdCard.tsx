import React from "react";

import { Card, Flex, Icon, Skeleton, Text } from "@gravity-ui/uikit";
import cx from "classnames";

import { AdType } from "@/shared/api";
import HumanIcon from "@/shared/assets/icons/human.svg";
import MetroIcon from "@/shared/assets/icons/metro.svg";
import { StateWrapperProps, withState } from "@/shared/hocs/withState";

import styles from "./AdCard.module.css";

interface AdCardStaticProps {
  imgSrc?: string;
  className?: string;
  onAdClick?: (id: number) => void;
}

interface AdCardNormalProps extends AdCardStaticProps {
  data: AdType & { imgSrc?: string };
}

type AdCardProps = StateWrapperProps<AdType> & AdCardStaticProps;

const AdCardLoading = () => {
  return <Skeleton className={styles.skeleton} />;
};

const AdCardNormal = (props: AdCardNormalProps) => {
  const { data: ad, className, onAdClick } = props;
  const { price, rooms, area, level, levels, street, imgSrc, id } = ad;
  const metro = ad["nearest-metro-station"];
  const apartmentType = ad["apartment-type"];
  const walkTime = ad["walk-time-to-metro"];
  const houseNumber = ad["house-number"];

  return (
    <Card className={cx(className, styles.root)}>
      <div
        onClick={() => {
          onAdClick?.(id);
        }}
      >
        <Flex direction="column" gap={2}>
          {imgSrc && <img className={styles.img} src={imgSrc} />}
          <Flex className={styles.description} direction="column" gap={1}>
            <Text variant="subheader-3">{`${price.toLocaleString(
              "ru"
            )} ₽`}</Text>
            <Flex gap={1}>
              <Text>
                {rooms}-комн. {apartmentType.toLowerCase()} · {area | 0} м² ·{" "}
                {level}/{levels} этаж
              </Text>
            </Flex>
            <Flex alignItems="center" gap={1}>
              <Icon data={MetroIcon} className={styles.metroIcon} />
              <Text>{metro}</Text>
              ·
              <Icon data={HumanIcon} className={styles.humanIcon} />
              <Text>{walkTime | 0} мин.</Text>
            </Flex>
            <Text>
              {street}, {houseNumber}
            </Text>
          </Flex>
        </Flex>
      </div>
    </Card>
  );
};

const StatefulAdCard = withState({
  Normal: AdCardNormal,
  Loading: AdCardLoading,
  Errored: () => <></>,
});

export const AdCard = (props: AdCardProps) => {
  return <StatefulAdCard {...props} />;
};

/* eslint-disable no-console */
import React, { useCallback, useState } from "react";

import { Button, Flex, Text, TextInput } from "@gravity-ui/uikit";
import { Link, useNavigate } from "react-router-dom";

import { notifyErrorDefault } from "@/shared/libs/notify";

import { useRegisterMutation } from "./api";

import styles from "./Register.module.css";

export const Register = () => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [againPassword, setAgainPassword] = useState("");
  const navigate = useNavigate();
  const { mutate: register, isPending } = useRegisterMutation({
    onSuccess: () => {
      navigate("/login");
    },
  });

  const onLoginUpdate = useCallback((updatedLogin: string) => {
    setLogin(updatedLogin);
  }, []);

  const onPasswordUpdate = useCallback((updatedPassword: string) => {
    setPassword(updatedPassword);
  }, []);

  const onAgainPasswordUpdate = useCallback((updatedAgainPassword: string) => {
    setAgainPassword(updatedAgainPassword);
  }, []);

  const onCountinue = useCallback(() => {
    if (!login.length || !password.length || !againPassword.length) {
      notifyErrorDefault({ content: "Поля не должны быть пустыми" });
      return;
    }
    if (password !== againPassword) {
      notifyErrorDefault({ content: "Пароли должны совпадать" });
      return;
    }
    register({ login, password, againPassword });
  }, [againPassword, login, password, register]);

  return (
    <Flex
      direction="column"
      className={styles.root}
      justifyContent="center"
      gap={3}
      alignItems="center"
    >
      <Text variant="display-3">Регистрация</Text>
      <Flex direction="column" className={styles.inputs} gap={1}>
        <TextInput
          name="login"
          placeholder="Логин"
          value={login}
          onUpdate={onLoginUpdate}
        />
        <TextInput
          name="password"
          placeholder="Пароль"
          type="password"
          value={password}
          onUpdate={onPasswordUpdate}
        />
        <TextInput
          name="againPassword"
          placeholder="Подтверждение пароля"
          type="password"
          value={againPassword}
          onUpdate={onAgainPasswordUpdate}
        />
      </Flex>
      <Flex
        justifyContent="space-between"
        alignItems="center"
        className={styles.actions}
      >
        <Link to="/login">Уже есть аккаунт?</Link>
        <Button loading={isPending} view="action" onClick={onCountinue}>
          Продолжить
        </Button>
      </Flex>
    </Flex>
  );
};

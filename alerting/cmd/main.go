package main

import (
	"alerting/component/config"
	"alerting/component/msgprocesser"
	"alerting/component/subscriber"
	"alerting/component/telegram"
	"context"
	"errors"
	"flag"
	"fmt"
	alertingLogger "github.com/apsdehal/go-logger"
	"github.com/nats-io/nats.go"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	cfgFilePath := flag.String("cfgfilepath", "configs/config.toml", "config.toml file path")
	flag.Parse()

	logger, err := alertingLogger.New("alerting-service", 1, os.Stdout)
	if err != nil {
		log.Fatalf("failed to init logger: %v", err)
	}

	cfg, err := config.New(*cfgFilePath)
	if err != nil {
		logger.Fatalf("failed to init config: %v", err)
	}

	logger.Noticef("cfg: %v", cfg)

	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()

	natsMessageDataCh := make(chan []byte, cfg.Nats.MsgPoolSize)
	natsSubscriptionCh := make(chan *nats.Subscription, 1)
	getMessageErrorCh := make(chan error, 1)
	processerAndSenderErrorCh := make(chan error, 1)
	done := make(chan struct{}, 1)

	go subscriber.SubscribeSubject(natsMessageDataCh, getMessageErrorCh, natsSubscriptionCh, done, cfg)

	tg, err := telegram.New(cfg.Telegram.ChatID, cfg.Telegram.TelegramToken)
	if err != nil {
		logger.Fatalf("failed to init tg bot api: %v", err)
	}

loop:
	for {
		select {
		case data := <-natsMessageDataCh:
			go func() {
				msg, significance, err := msgprocesser.MsgProcess(data)
				if err != nil {
					processerAndSenderErrorCh <- fmt.Errorf("msg processing: %w", err)
					return
				}

				err = tg.SendErrorAlert(msg, significance)
				if err != nil {
					processerAndSenderErrorCh <- fmt.Errorf("tg msg alert sending: %w", err)
					return
				}
			}()

		case err = <-getMessageErrorCh:
			logger.Errorf("msg getting from nats subject: %v", err)

			done <- struct{}{}

			break loop

		case err = <-processerAndSenderErrorCh:
			logger.Errorf("msg processing or tg alert sending: %v", err)

			done <- struct{}{}

			break loop

		case <-ctx.Done():
			subscription := <-natsSubscriptionCh
			err := subscription.Unsubscribe()
			if err != nil {
				if errors.Is(err, nats.ErrConnectionClosed) {
					logger.Warningf("nats connection closing")
				} else {
					logger.Fatalf("failed to unsubscribe: %v", err)
				}
			}

			return
		}
	}
}

import { HttpClient, UseRegisterMutationParams } from "@/shared/api";
import {
  UseCustomMutationOptions,
  useMutation,
} from "@/shared/hooks/useMutation";

export const useRegisterMutation = (options?: UseCustomMutationOptions) => {
  const mutation = useMutation({
    mutationOptions: {
      ...options,
      mutationFn: (params: UseRegisterMutationParams) =>
        HttpClient.register(params),
    },
    errorToastOptions: {
      content: "Не удалось создать аккаунт",
    },
  });

  return mutation;
};

package domain

type ErrorMessage struct {
	Msg          string `json:"msg"`
	Significance string `json:"significance"`
}

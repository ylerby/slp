package config

import (
	"fmt"
	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	Nats     Nats `toml:"Nats"`
	Telegram Telegram
}

type Nats struct {
	NatsPort    string `env:"NATS_CLIENT_PORT"`
	NatsHost    string `env:"NATS_HOST"`
	NatsSubject string `toml:"NatsSubject"`
	MsgPoolSize int    `toml:"MsgPoolSize"`
}

type Telegram struct {
	TelegramToken string `env:"TELEGRAM_TOKEN"`
	ChatID        int64  `env:"CHAT_ID"`
}

func New(path string) (*Config, error) {
	cfg := new(Config)

	if err := cleanenv.ReadConfig(path, cfg); err != nil {
		return nil, fmt.Errorf("reading config from %s: %w", path, err)
	}

	return cfg, nil
}

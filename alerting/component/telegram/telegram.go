package telegram

import (
	"fmt"
	telegram "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"time"
)

type Telegram struct {
	tgBotAPI *telegram.BotAPI
	chatID   int64
}

func New(chatID int64, token string) (*Telegram, error) {
	botAPI, err := telegram.NewBotAPI(token)
	if err != nil {
		return nil, fmt.Errorf("telegram bot api creating: %w", err)
	}

	return &Telegram{
		tgBotAPI: botAPI,
		chatID:   chatID,
	}, nil
}

func (tg *Telegram) SendErrorAlert(errorText, errorSignificance string) error {
	loc, err := time.LoadLocation("Europe/Moscow")
	if err != nil {
		return fmt.Errorf("chat timezone choosing: %w", err)
	}

	msg := fmt.Sprintf(`
		[ALERT]

		SLP backend error occurred:

		Error significance: %s

		Error message: %s

		Error occurred at: %v
	`, errorSignificance, errorText, time.Now().In(loc).Format(time.RFC1123))

	msgCfg := telegram.NewMessage(tg.chatID, msg)

	_, err = tg.tgBotAPI.Send(msgCfg)
	if err != nil {
		return fmt.Errorf("msg sending: %w", err)
	}

	return nil
}

package domain

type ErrorMessage struct {
	Msg          string `json:"msg"`
	Significance string `json:"significance"`
}

type LoginFields struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type LoginResponseBody struct {
	Token string `json:"token"`
}

type RegisterFields struct {
	Login         string `json:"login"`
	Password      string `json:"password"`
	AgainPassword string `json:"again-password"`
}

type RealEstate struct {
	Region                      string  `json:"region"`
	BuildingType                string  `json:"building-type"`
	ObjectType                  string  `json:"object-type"`
	City                        string  `json:"city"`
	OsmCityNearestName          string  `json:"osm-city-nearest-name"`
	HouseNumber                 string  `json:"house-number"`
	Street                      string  `json:"street"`
	ApartmentType               string  `json:"apartment-type"`
	NearestMetroStation         string  `json:"nearest-metro-station"`
	OsmLeisurePointsIn0Dot01    int     `gorm:"column:osm_leisure_points_in_001" json:"osm-leisure-points-in-001"`
	OsmTransportStopClosestDist float64 `json:"osm-transport-stop-closest-dist"`
	Rooms                       int     `json:"rooms"`
	Area                        float64 `json:"area"`
	KitchenArea                 float64 `json:"kitchen-area"`
	OsmAmenityPointsIn0Dot01    int     `gorm:"column:osm_amenity_points_in_001" json:"osm-amenity-points-in-001"`
	OsmBuildingPointsIn0Dot01   int     `gorm:"column:osm_building_points_in_001" json:"osm-building-points-in-001"`
	OsmCateringPointIn0Dot01    int     `gorm:"column:osm_catering_point_in_001" json:"osm-catering-point-in-001"`
	OsmCityClosestDist          float64 `json:"osm-city-closest-dist"`
	OsmCityNearestPopulation    float64 `json:"osm-city-nearest-population"`
	OsmCrossingClosestDist      float64 `json:"osm-crossing-closest-dist"`
	OsmCulturePointsIn0Dot01    int     `gorm:"column:osm_culture_points_in_001" json:"osm-culture-points-in-001"`
	OsmFinancePointsIn0Dot01    int     `gorm:"column:osm_finance_points_in_001" json:"osm-finance-points-in-001"`
	OsmHistoricPointsIn0Dot01   int     `gorm:"column:osm_historic_points_in_001" json:"osm-historic-points-in-001"`
	OsmHotelsPointsIn0Dot01     int     `gorm:"column:osm_hotels_points_in_001" json:"osm-hotels-points-in-001"`
	Level                       int     `json:"level"`
	OsmOfficesPointsIn0Dot01    int     `gorm:"column:osm_offices_points_in_001" json:"osm-offices-points-in-001"`
	OsmShopsPointsIn0Dot01      int     `gorm:"column:osm_shops_points_in_001" json:"osm-shops-points-in-001"`
	OsmSubwayClosestDist        float64 `json:"osm-subway-closest-dist"`
	OsmTrainStopClosestDist     float64 `json:"osm-train-stop-closest-dist"`
	OsmTrainStopPointsIn0Dot01  int     `gorm:"column:osm_train_stop_points_in_001" json:"osm-train-stop-points-in-001"`
	Levels                      int     `json:"levels"`
	ReformCountOfHouses1K       int     `json:"reform-count-of-houses-1k"`
	ReformCountOfHouses500      int     `json:"reform-count-of-houses-500"`
	ManyOffices                 int     `json:"many-offices"`
	ManyFood                    int     `json:"many-food"`
	ManyShops                   int     `json:"many-shops"`
	ManyFinancialOrganizations  int     `json:"many-financial-organizations"`
	ManyEntertainment           int     `json:"many-entertainment"`
	ManyHistoricalObjects       int     `json:"many-historical-objects"`
	ManyHotels                  int     `json:"many-hotels"`
	StationRate                 int     `json:"station-rate"`
	ManyStations                int     `json:"many-stations"`
	ManyCultureObjects          int     `json:"many-culture-objects"`
	ManyComfortObjects          int     `json:"many-comfort-objects"`
	KremlinDist                 float64 `json:"kremlin-dist"`
	GeoLon                      float64 `json:"geo-lon"`
	MetroDist                   float64 `json:"metro-dist"`
	WalkTimeToMetro             float64 `json:"walk-time-to-metro"`
	GeoLat                      float64 `json:"geo-lat"`
	Price                       float64 `json:"price"`
	ID                          int     `json:"id"`
}

type UpdateRecordFields struct {
	Region                      string  `json:"region"`
	BuildingType                string  `json:"building-type"`
	ObjectType                  string  `json:"object-type"`
	City                        string  `json:"city"`
	OsmCityNearestName          string  `json:"osm-city-nearest-name"`
	HouseNumber                 string  `json:"house-number"`
	Street                      string  `json:"street"`
	ApartmentType               string  `json:"apartment-type"`
	NearestMetroStation         string  `json:"nearest-metro-station"`
	OsmLeisurePointsIn0Dot01    int     `gorm:"column:osm_leisure_points_in_001" json:"osm-leisure-points-in-001"`
	OsmTransportStopClosestDist float64 `json:"osm-transport-stop-closest-dist"`
	Rooms                       int     `json:"rooms"`
	Area                        float64 `json:"area"`
	KitchenArea                 float64 `json:"kitchen-area"`
	OsmAmenityPointsIn0Dot01    int     `gorm:"column:osm_amenity_points_in_001" json:"osm-amenity-points-in-001"`
	OsmBuildingPointsIn0Dot01   int     `gorm:"column:osm_building_points_in_001" json:"osm-building-points-in-001"`
	OsmCateringPointIn0Dot01    int     `gorm:"column:osm_catering_point_in_001" json:"osm-catering-point-in-001"`
	OsmCityClosestDist          float64 `json:"osm-city-closest-dist"`
	OsmCityNearestPopulation    float64 `json:"osm-city-nearest-population"`
	OsmCrossingClosestDist      float64 `json:"osm-crossing-closest-dist"`
	OsmCulturePointsIn0Dot01    int     `gorm:"column:osm_culture_points_in_001" json:"osm-culture-points-in-001"`
	OsmFinancePointsIn0Dot01    int     `gorm:"column:osm_finance_points_in_001" json:"osm-finance-points-in-001"`
	OsmHistoricPointsIn0Dot01   int     `gorm:"column:osm_historic_points_in_001" json:"osm-historic-points-in-001"`
	OsmHotelsPointsIn0Dot01     int     `gorm:"column:osm_hotels_points_in_001" json:"osm-hotels-points-in-001"`
	Level                       int     `json:"level"`
	OsmOfficesPointsIn0Dot01    int     `gorm:"column:osm_offices_points_in_001" json:"osm-offices-points-in-001"`
	OsmShopsPointsIn0Dot01      int     `gorm:"column:osm_shops_points_in_001" json:"osm-shops-points-in-001"`
	OsmSubwayClosestDist        float64 `json:"osm-subway-closest-dist"`
	OsmTrainStopClosestDist     float64 `json:"osm-train-stop-closest-dist"`
	OsmTrainStopPointsIn0Dot01  int     `gorm:"column:osm_train_stop_points_in_001" json:"osm-train-stop-points-in-001"`
	Levels                      int     `json:"levels"`
	ReformCountOfHouses1K       int     `json:"reform-count-of-houses-1k"`
	ReformCountOfHouses500      int     `json:"reform-count-of-houses-500"`
	ManyOffices                 int     `json:"many-offices"`
	ManyFood                    int     `json:"many-food"`
	ManyShops                   int     `json:"many-shops"`
	ManyFinancialOrganizations  int     `json:"many-financial-organizations"`
	ManyEntertainment           int     `json:"many-entertainment"`
	ManyHistoricalObjects       int     `json:"many-historical-objects"`
	ManyHotels                  int     `json:"many-hotels"`
	StationRate                 int     `json:"station-rate"`
	ManyStations                int     `json:"many-stations"`
	ManyCultureObjects          int     `json:"many-culture-objects"`
	ManyComfortObjects          int     `json:"many-comfort-objects"`
	KremlinDist                 float64 `json:"kremlin-dist"`
	GeoLon                      float64 `json:"geo-lon"`
	MetroDist                   float64 `json:"metro-dist"`
	WalkTimeToMetro             float64 `json:"walk-time-to-metro"`
	GeoLat                      float64 `json:"geo-lat"`
	Price                       float64 `json:"price"`
	ID                          int     `json:"id"`
}

type CreateRecordFields struct {
	Region                      string  `json:"region"`
	BuildingType                string  `json:"building-type"`
	ObjectType                  string  `json:"object-type"`
	City                        string  `json:"city"`
	OsmCityNearestName          string  `json:"osm-city-nearest-name"`
	HouseNumber                 string  `json:"house-number"`
	Street                      string  `json:"street"`
	ApartmentType               string  `json:"apartment-type"`
	NearestMetroStation         string  `json:"nearest-metro-station"`
	OsmOfficesPointsIn0Dot01    int     `gorm:"column:osm_offices_points_in_001" json:"osm-offices-points-in-001"`
	ReformCountOfHouses1K       int     `json:"reform-count-of-houses-1k"`
	Area                        float64 `json:"area"`
	KitchenArea                 float64 `json:"kitchen-area"`
	OsmAmenityPointsIn0Dot01    int     `gorm:"column:osm_amenity_points_in_001" json:"osm-amenity-points-in-001"`
	OsmBuildingPointsIn0Dot01   int     `gorm:"column:osm_building_points_in_001" json:"osm-building-points-in-001"`
	OsmCateringPointIn0Dot01    int     `gorm:"column:osm_catering_point_in_001" json:"osm-catering-point-in-001"`
	OsmCityClosestDist          float64 `json:"osm-city-closest-dist"`
	OsmCityNearestPopulation    float64 `json:"osm-city-nearest-population"`
	OsmCrossingClosestDist      float64 `json:"osm-crossing-closest-dist"`
	OsmCulturePointsIn0Dot01    int     `gorm:"column:osm_culture_points_in_001" json:"osm-culture-points-in-001"`
	OsmFinancePointsIn0Dot01    int     `gorm:"column:osm_finance_points_in_001" json:"osm-finance-points-in-001"`
	OsmHistoricPointsIn0Dot01   int     `gorm:"column:osm_historic_points_in_001" json:"osm-historic-points-in-001"`
	OsmHotelsPointsIn0Dot01     int     `gorm:"column:osm_hotels_points_in_001" json:"osm-hotels-points-in-001"`
	OsmLeisurePointsIn0Dot01    int     `gorm:"column:osm_leisure_points_in_001" json:"osm-leisure-points-in-001"`
	Levels                      int     `json:"levels"`
	OsmShopsPointsIn0Dot01      int     `gorm:"column:osm_shops_points_in_001" json:"osm-shops-points-in-001"`
	OsmSubwayClosestDist        float64 `json:"osm-subway-closest-dist"`
	OsmTrainStopClosestDist     float64 `json:"osm-train-stop-closest-dist"`
	OsmTrainStopPointsIn0Dot01  int     `gorm:"column:osm_train_stop_points_in_001" json:"osm-train-stop-points-in-001"`
	OsmTransportStopClosestDist float64 `json:"osm-transport-stop-closest-dist"`
	Rooms                       int     `json:"rooms"`
	ReformCountOfHouses500      int     `json:"reform-count-of-houses-500"`
	ManyOffices                 int     `json:"many-offices"`
	ManyFood                    int     `json:"many-food"`
	ManyShops                   int     `json:"many-shops"`
	ManyFinancialOrganizations  int     `json:"many-financial-organizations"`
	ManyEntertainment           int     `json:"many-entertainment"`
	ManyHistoricalObjects       int     `json:"many-historical-objects"`
	ManyHotels                  int     `json:"many-hotels"`
	StationRate                 int     `json:"station-rate"`
	ManyStations                int     `json:"many-stations"`
	ManyCultureObjects          int     `json:"many-culture-objects"`
	ManyComfortObjects          int     `json:"many-comfort-objects"`
	KremlinDist                 float64 `json:"kremlin-dist"`
	Level                       int     `json:"level"`
	MetroDist                   float64 `json:"metro-dist"`
	WalkTimeToMetro             float64 `json:"walk-time-to-metro"`
	GeoLon                      float64 `json:"geo-lon"`
	GeoLat                      float64 `json:"geo-lat"`
	Price                       float64 `json:"price"`
}

type DeleteRecordFields struct {
	ID int `json:"id"`
}

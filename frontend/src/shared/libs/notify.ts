import { ToastProps } from "@gravity-ui/uikit";
import { toaster } from "@gravity-ui/uikit/toaster-singleton-react-18";

import { UNKNOWN_ERROR } from "../constants/error";

import { getUid } from "./uid";

function getCommonToastProps(): ToastProps {
  return {
    name: getUid(),
    autoHiding: false,
    className: "text-break",
  };
}

function notifyDefault(toastProps: Partial<ToastProps>) {
  toaster.add({
    ...getCommonToastProps(),
    ...toastProps,
  });
}

export function notifySuccessDefault(toastProps: Partial<ToastProps>) {
  notifyDefault({ type: "success", title: "Успех", ...toastProps });
}

export function notifyErrorDefault(toastProps?: Partial<ToastProps>) {
  notifyDefault({
    type: "error",
    title: "Ошибка",
    content: UNKNOWN_ERROR,
    ...toastProps,
  });
}

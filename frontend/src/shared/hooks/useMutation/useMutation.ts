/* eslint-disable no-console */
import { useCallback } from "react";

import { ToastProps } from "@gravity-ui/uikit";
import {
  UseMutationOptions,
  useMutation as useBaseMutation,
} from "@tanstack/react-query";
import { AxiosError } from "axios";

import { UNKNOWN_ERROR } from "@/shared/constants/error";
import { notifyErrorDefault, notifySuccessDefault } from "@/shared/libs/notify";

export interface UseCustomMutationOptions<
  TData = unknown,
  TError = AxiosError,
  TVariables = unknown,
  TContext = unknown
> extends Omit<
    UseMutationOptions<TData, TError, TVariables, TContext>,
    "onSuccess" | "onError"
  > {
  onSuccess?:
    | ((
        data: TData,
        variables: TVariables,
        context: TContext,
        defaultOnSuccess: () => void
      ) => unknown)
    | undefined;
  onError?:
    | ((
        error: TError,
        variables: TVariables,
        context: TContext | undefined,
        defaultOnError: () => void
      ) => unknown)
    | undefined;
}

interface UseMutationPayload<
  TData,
  TError extends AxiosError,
  TVariables,
  TContext
> {
  mutationOptions: UseCustomMutationOptions<
    TData,
    TError,
    TVariables,
    TContext
  >;
  successToastOptions?: Partial<ToastProps>;
  errorToastOptions?: Partial<ToastProps>;
  withSuccessNotification?: boolean;
  withErrorNotification?: boolean;
}

export function useMutation<
  TData,
  TError extends AxiosError,
  TVariables,
  TContext
>(payload: UseMutationPayload<TData, TError, TVariables, TContext>) {
  const {
    mutationOptions,
    successToastOptions,
    errorToastOptions,
    withErrorNotification = true,
    withSuccessNotification = true,
  } = payload;
  const {
    onSuccess: onSuccessBase,
    onError: onErrorBase,
    ...otherMutationOptions
  } = mutationOptions;

  const defaultOnSuccess = useCallback(() => {
    notifySuccessDefault(successToastOptions ?? {});
  }, [successToastOptions]);

  const defaultOnError = useCallback(
    (error?: string) => {
      notifyErrorDefault({ content: error, ...errorToastOptions } ?? {});
    },
    [errorToastOptions]
  );

  const onSuccess = useCallback(
    (data: TData, variables: TVariables, context: TContext) => {
      if (onSuccessBase) {
        onSuccessBase(data, variables, context, defaultOnSuccess);
      } else {
        if (!withSuccessNotification) {
          return;
        }
        defaultOnSuccess();
      }
    },
    [defaultOnSuccess, onSuccessBase, withSuccessNotification]
  );

  const onError = useCallback(
    (error: TError, variables: TVariables, context: TContext | undefined) => {
      if (onErrorBase) {
        onErrorBase(error, variables, context, defaultOnError);
      } else {
        if (!withErrorNotification) {
          return;
        }

        let message = UNKNOWN_ERROR;
        if (typeof error?.response?.data === "string") {
          message = error?.response?.data;
        } else if (typeof (error?.response?.data as any)?.detail === "string") {
          message = (error?.response?.data as any)?.detail;
        }

        defaultOnError(message);
      }
    },
    [defaultOnError, onErrorBase, withErrorNotification]
  );

  const mutation = useBaseMutation({
    ...otherMutationOptions,
    onSuccess,
    onError,
  });

  return mutation;
}

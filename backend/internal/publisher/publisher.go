package publisher

import (
	"fmt"
	"github.com/apsdehal/go-logger"
	"github.com/goccy/go-json"
	"github.com/nats-io/nats.go"
	"gitlab.com/ylerby/slp/internal/domain"
)

type Publisher struct {
	nc *nats.Conn
	l  *logger.Logger

	subject string
}

func New(host, port, subject string, l *logger.Logger) (*Publisher, error) {
	publisher := new(Publisher)

	nc, err := nats.Connect(fmt.Sprintf("http://%s:%s", host, port))
	if err != nil {
		return nil, fmt.Errorf("publisher connection: %w", err)
	}

	publisher.nc = nc
	publisher.subject = subject
	publisher.l = l

	return publisher, nil
}

func (pb *Publisher) PublishMsg(errorText, errorSignificance string) {
	msg := domain.ErrorMessage{
		Msg:          errorText,
		Significance: errorSignificance,
	}

	data, err := json.Marshal(&msg)
	if err != nil {
		pb.l.Errorf("json marshalling: %v", err)
		return
	}

	err = pb.nc.Publish(pb.subject, data)
	if err != nil {
		pb.l.Errorf("msg publishing: %v", err)
		return
	}
}

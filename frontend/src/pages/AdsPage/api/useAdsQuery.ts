/* eslint-disable no-console */
import { useQuery } from "@tanstack/react-query";

import { HttpClient } from "@/shared/api";

export const useAdsQuery = () => {
  const query = useQuery({
    queryFn: HttpClient.getAds,
    queryKey: ["getAds"],
    select: (data) => data.data,
    gcTime: 0,
  });

  return query;
};

package msgprocesser

import (
	"alerting/component/domain"
	"fmt"
	"github.com/goccy/go-json"
)

func MsgProcess(data []byte) (string, string, error) {
	msg := new(domain.ErrorMessage)

	err := json.Unmarshal(data, msg)
	if err != nil {
		return "", "", fmt.Errorf("msg data unmarshalling: %w", err)
	}

	return msg.Msg, msg.Significance, nil
}

/* eslint-disable react/prop-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-console */
import React, { useEffect, useMemo, useState } from "react";

import {
  Button,
  Dialog,
  Flex,
  Select,
  Text,
  TextInput,
} from "@gravity-ui/uikit";
import { useFormik } from "formik";
import { useParams, useSearchParams } from "react-router-dom";
import * as Yup from "yup";

import {
  apartmentTypeOptions,
  buildingOptions,
  metroOptions,
  objectTypeOptions,
  regionOptions,
} from "../FilterAds";

import { usePredictMutation } from "./api";
import { useCreateAdMutation } from "./api/useCreate";
import { useDeleteAdMutation } from "./api/useDelete";
import { useUpdateAdMutation } from "./api/useUpdate";

const testStringBeforeInitialValues = {
  "house-number": "9",
  street: "Панфилова",
  city: "Москва",
};

const emptyStringBeforeInitialValues = {
  "house-number": "", //
  street: "", //
  city: "", //
};

const testNumberBeforeInitialValues = {
  level: "30",
  levels: "80",
  rooms: "2",
  area: "58.8125",
  "kitchen-area": "32.796875",
};

const emptyNumberBeforeInitialValues = {
  level: "", //
  levels: "", //
  rooms: "", //
  area: "", //
  "kitchen-area": "", //
};

const testSelectBeforeInitialValues = {
  region: "Москва",
  "building-type": "Монолитный",
  "object-type": "Вторичное",
  "apartment-type": "Квартира",
};

const emptySelectBeforeInitialValues = {
  region: "", //
  "object-type": "", //
  "apartment-type": "", //
  "building-type": "", //
};

const stringAfterInitialValues = {
  "osm-city-nearest-name": "",
};

const numberAfterInitialValues = {
  price: "",
  "geo-lat": "",
  "geo-lon": "",
  "metro-dist": "",
  "walk-time-to-metro": "",
  "many-offices": "",
  "many-food": "",
  "many-shops": "",
  "many-financial-organizations": "",
  "many-entertainment": "",
  "many-historical-objects": "",
  "many-hotels": "",
  "station-rate": "",
  "many-stations": "",
  "many-culture-objects": "",
  "many-comfort-objects": "",
  "osm-amenity-points-in-001": "",
  "osm-building-points-in-001": "",
  "osm-catering-point-in-001": "",
  "osm-city-closest-dist": "",
  "osm-city-nearest-population": "",
  "osm-crossing-closest-dist": "",
  "osm-culture-points-in-001": "",
  "osm-finance-points-in-001": "",
  "osm-historic-points-in-001": "",
  "osm-hotels-points-in-001": "",
  "osm-leisure-points-in-001": "",
  "osm-offices-points-in-001": "",
  "osm-shops-points-in-001": "",
  "osm-subway-closest-dist": "",
  "osm-train-stop-closest-dist": "",
  "osm-train-stop-points-in-001": "",
  "osm-transport-stop-closest-dist": "",
  "reform-count-of-houses-1k": "",
  "reform-count-of-houses-500": "",
};

const selectAfterInitialValues = {
  "nearest-metro-station": "",
};

const nameMap = {
  geo_lat: "Широта",
  geo_lon: "Долгота",
  region: "Регион",
  city: "Город",
  building_type: "Тип здания",
  level: "Этаж",
  levels: "Этажей в доме",
  rooms: "Количество комнат",
  area: "Площадь",
  kitchen_area: "Площадь кухни",
  object_type: "Тип объекта",
  apartment_type: "Тип квартиры",
  street: "Улица",
  house_number: "Номер дома",
  nearest_metro_station: "Метро",
  station_rate: "Отношение железнодорожных станций",
  metro_dist: "Расстояние до метро",
  walk_time_to_metro: "Время пешком до метро",
  many_offices: "Много офисов",
  many_food: "Много ресторанов",
  many_shops: "Много магазинов",
  many_financial_organizations: "Много финансовых организаций",
  many_entertainment: "Много мест для развлекательния",
  many_historical_objects: "Много исторических объектов",
  many_hotels: "Много отелей",
  many_stations: "Много транспорта",
  many_culture_objects: "Много культурных объектов",
  many_comfort_objects: "Много комфортных объектов",
  price: "Цена",
  osm_catering_points_in_001: "Количество точек питания в радиусе 1 км",
  osm_city_closest_dist: "Расстояние до ближайшего города",
  osm_city_nearest_name: "Название ближайшего города",
  osm_culture_points_in_001:
    "Количество точек культурных объектов в радиусе 1 км",
  osm_finance_points_in_001:
    "Количество точек финансовых организаций в радиусе 1 км",
  osm_historic_points_in_001:
    "Количество точек исторических объектов в радиусе 1 км",
  osm_hotels_points_in_001: "Количество точек отелей в радиусе 1 км",
  osm_leisure_points_in_001: "Количество точек объектов отдыха в радиусе 1 км",
  osm_offices_points_in_001: "Количество точек офисов в радиусе 1 км",
  "osm_shops_points_in_001 ": "Количество точек магазинов в радиусе 1 км",
  osm_train_stop_closest_dist:
    "Расстояние до ближайшей железнодорожной станции",
  osm_train_stop_points_in_001:
    "Количество точек железнодорожных станций в радиусе 1 км",
};

const options = {
  region: regionOptions,
  "building-type": buildingOptions,
  "apartment-type": apartmentTypeOptions,
  "object-type": objectTypeOptions,
  "nearest-metro-station": metroOptions,
};

const getSnakeCase = (str) => str.replaceAll("-", "_");

const getKebabCase = (str) => str.replaceAll("_", "-");

Object.fromEntries(
  Object.keys(emptyStringBeforeInitialValues).map((key) => [
    key,
    Yup.string().required("Строка. Обязательное поле"),
  ])
);

const validationSchema = Yup.object({
  ...Object.fromEntries(
    Object.keys(emptyStringBeforeInitialValues).map((key) => [
      key,
      Yup.string().required("Строка. Обязательное поле"),
    ])
  ),
  ...Object.fromEntries(
    Object.keys(emptyNumberBeforeInitialValues).map((key) => [
      key,
      Yup.number().required("Число. Обязательное поле"),
    ])
  ),
  ...Object.fromEntries(
    Object.keys(emptySelectBeforeInitialValues).map((key) => [
      key,
      Yup.string().required("Строка. Обязательное поле"),
    ])
  ),
  ...Object.fromEntries(
    Object.keys(stringAfterInitialValues).map((key) => [
      key,
      Yup.string().required("Строка. Обязательное поле"),
    ])
  ),
  ...Object.fromEntries(
    Object.keys(numberAfterInitialValues).map((key) => [
      key,
      Yup.number().required("Число. Обязательное поле"),
    ])
  ),
  ...Object.fromEntries(
    Object.keys(selectAfterInitialValues).map((key) => [
      key,
      Yup.string().required("Строка. Обязательное поле"),
    ])
  ),
});

export const CreateAd = ({
  open,
  onClose,
  isEdit = false,
  values,
  onSuccess,
}) => {
  const [searchParams] = useSearchParams();
  const [isPredicted, setIsPredicted] = useState(false);
  const [params] = useSearchParams();
  const isTest = params.get("test") !== null;
  const isView = params.get("view") !== null;

  const stringBeforeInitialValues = useMemo(() => {
    return isTest
      ? testStringBeforeInitialValues
      : emptyStringBeforeInitialValues;
  }, [isTest]);

  const numberBeforeInitialValues = useMemo(() => {
    return isTest
      ? testNumberBeforeInitialValues
      : emptyNumberBeforeInitialValues;
  }, [isTest]);

  const selectBeforeInitialValues = useMemo(() => {
    return isTest
      ? testSelectBeforeInitialValues
      : emptySelectBeforeInitialValues;
  }, [isTest]);

  const filterValuesByValues = (initialValues) => {
    if (!values) {
      return {};
    }
    const keys = Object.keys(initialValues);
    return Object.fromEntries(
      Object.entries(values).filter(([key]) => keys.includes(key))
    );
  };

  const getInitialValues = (initialValues) => {
    return values ? filterValuesByValues(initialValues) : initialValues;
  };

  const stringBeforeFormik = useFormik({
    initialValues: stringBeforeInitialValues,
    validationSchema,
    onSubmit: (values) => console.log(values),
  });

  const numberBeforeFormik = useFormik({
    initialValues: numberBeforeInitialValues,
    validationSchema,
    onSubmit: (values) => console.log(values),
  });

  const selectBeforeFormik = useFormik({
    initialValues: selectBeforeInitialValues,
    validationSchema,
    onSubmit: (values) => console.log(values),
  });

  const stringAfterFormik = useFormik({
    initialValues: stringAfterInitialValues,
    validationSchema,
    onSubmit: (values) => console.log(values),
  });

  const numberAfterFormik = useFormik({
    initialValues: numberAfterInitialValues,
    validationSchema,
    onSubmit: (values) => console.log(values),
  });

  const selectAfterFormik = useFormik({
    initialValues: selectAfterInitialValues,
    validationSchema,
    onSubmit: (values) => console.log(values),
  });

  const castObjectToSnakeCase = (obj) => {
    return Object.fromEntries(
      Object.entries(obj).map(([key, value]) => [getSnakeCase(key), value])
    );
  };

  const castObjectValuesKebabCase = (obj) => {
    return Object.fromEntries(
      Object.entries(obj).map(([key, value]) => [getKebabCase(key), value])
    );
  };

  const castObjectValuesNumbers = (obj) => {
    return Object.fromEntries(
      Object.entries(obj).map(([key, value]) => [key, parseFloat(value)])
    );
  };

  const castObjectValuesStrings = (obj) => {
    return Object.fromEntries(
      Object.entries(obj).map(([key, value]) => [key, String(value)])
    );
  };

  const getInputValue = (formik, key) => {
    return formik.values[key];
  };

  const getSelectValue = (formik, key) => {
    return formik.values[key] ? [formik.values[key]] : undefined;
  };

  const onPredictSuccess = (data) => {
    const obj = castObjectValuesKebabCase(data.data.data[0]);
    const stringKeys = Object.keys(obj).filter((key) =>
      Object.keys(stringAfterInitialValues).includes(key)
    );
    const numberKeys = Object.keys(obj).filter((key) =>
      Object.keys(numberAfterInitialValues).includes(key)
    );
    const selectKeys = Object.keys(obj).filter((key) =>
      Object.keys(selectAfterInitialValues).includes(key)
    );

    const values = Object.entries(obj);
    const stringValues = Object.fromEntries(
      values.filter(([key]) => stringKeys.includes(key))
    );
    const numberValues = castObjectValuesStrings(
      Object.fromEntries(values.filter(([key]) => numberKeys.includes(key)))
    );
    const selectValues = Object.fromEntries(
      values.filter(([key]) => selectKeys.includes(key))
    );

    stringAfterFormik.setValues(stringValues);
    numberAfterFormik.setValues(numberValues);
    selectAfterFormik.setValues(selectValues);
    setIsPredicted(true);
  };

  const onPredictError = (error, variables, context, defaultOnError) => {
    defaultOnError(error?.response?.data?.detail);
    setIsPredicted(true);
  };

  const { mutate: predict, isPending: isPredictPending } = usePredictMutation({
    onSuccess: onPredictSuccess,
    onError: onPredictError,
  });

  const getBeforeValuesKebabCase = () => {
    const stringValues = castObjectValuesKebabCase(stringBeforeFormik.values);
    const numberValues = castObjectValuesNumbers(
      castObjectValuesKebabCase(numberBeforeFormik.values)
    );
    const selectValues = castObjectValuesKebabCase(selectBeforeFormik.values);
    const values = { ...stringValues, ...numberValues, ...selectValues };
    return values;
  };

  const getAfterValuesKebabCase = () => {
    const stringValues = castObjectValuesKebabCase(stringAfterFormik.values);
    const numberValues = castObjectValuesNumbers(
      castObjectValuesKebabCase(numberAfterFormik.values)
    );
    const selectValues = castObjectValuesKebabCase(selectAfterFormik.values);
    const values = { ...stringValues, ...numberValues, ...selectValues };
    return values;
  };

  const getAllValuesKebabCase = () => {
    const result = {
      ...getBeforeValuesKebabCase(),
      ...getAfterValuesKebabCase(),
    };
    if (values?.id !== undefined) {
      result.id = values.id;
    }
    return result;
  };

  const onPredict = () => {
    const stringValues = castObjectToSnakeCase(stringBeforeFormik.values);
    const numberValues = castObjectValuesNumbers(
      castObjectToSnakeCase(numberBeforeFormik.values)
    );
    const selectValues = castObjectToSnakeCase(selectBeforeFormik.values);
    const values = { ...stringValues, ...numberValues, ...selectValues };
    predict(values);
  };

  useEffect(() => {
    if (open) {
      setIsPredicted(false);
      stringBeforeFormik.setValues(getInitialValues(stringBeforeInitialValues));
      numberBeforeFormik.setValues(getInitialValues(numberBeforeInitialValues));
      selectBeforeFormik.setValues(getInitialValues(selectBeforeInitialValues));
      stringAfterFormik.setValues(getInitialValues(stringAfterInitialValues));
      numberAfterFormik.setValues(getInitialValues(numberAfterInitialValues));
      selectAfterFormik.setValues(getInitialValues(selectAfterInitialValues));
    }
  }, [open]);

  const onUpadateSuccess = (data, variables, context, defaultOnSuccess) => {
    defaultOnSuccess();
    onClose();
    onSuccess();
  };

  const { mutate: update, isPending: isUpdatePending } = useUpdateAdMutation({
    onSuccess: onUpadateSuccess,
  });

  const onCreateSuccess = (data, variables, context, defaultOnSuccess) => {
    defaultOnSuccess();
    onClose();
    onSuccess();
  };

  const { mutate: create, isPending: isCreatePending } = useCreateAdMutation({
    onSuccess: onCreateSuccess,
  });

  const { mutate: deleteAd, isPending: isDeletePending } = useDeleteAdMutation({
    onSuccess: onCreateSuccess,
  });

  const onNextClick = () => {
    const values = getAllValuesKebabCase();
    if (isEdit) {
      update(values);
    } else {
      create(values);
    }
  };

  const onDelete = () => {
    const id = parseInt(searchParams.get("id"));

    if (Object.is(NaN, id)) {
      return;
    }

    deleteAd(id);
  };

  const isAllDisabled = isUpdatePending || isCreatePending || isDeletePending;
  const isAfterActionsDisabled = (!isPredicted && !isEdit) || isAllDisabled;
  const isNextButtonDisabled = !isPredicted && !isEdit && !isAllDisabled;

  const getTitle = () => {
    if (isView) {
      return "Просмотр объявления";
    }
    if (isEdit) {
      return "Редактирование объявления";
    }
    return "Создание объявления";
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <Flex
        direction="column"
        style={{ width: "60vw", padding: "50px" }}
        gap={2}
      >
        <Flex justifyContent="center">
          <Text variant="display-1">{getTitle()}</Text>
        </Flex>
        {Object.keys(stringBeforeInitialValues).map(
          (key) =>
            nameMap[getSnakeCase(key)] && (
              <TextInput
                key={key}
                disabled={isAllDisabled || isView}
                id={key}
                name={key}
                type="text"
                error={stringBeforeFormik.errors[key]}
                label={nameMap[getSnakeCase(key)]}
                value={getInputValue(stringBeforeFormik, key)}
                onChange={stringBeforeFormik.handleChange}
              />
            )
        )}

        {Object.keys(numberBeforeInitialValues).map(
          (key) =>
            nameMap[getSnakeCase(key)] && (
              <TextInput
                key={key}
                disabled={isAllDisabled || isView}
                id={key}
                name={key}
                error={numberBeforeFormik.errors[key]}
                type="number"
                label={nameMap[getSnakeCase(key)]}
                value={getInputValue(numberBeforeFormik, key)}
                onChange={numberBeforeFormik.handleChange}
              />
            )
        )}

        {Object.keys(selectBeforeInitialValues).map(
          (key) =>
            nameMap[getSnakeCase(key)] && (
              <Select
                disabled={isAllDisabled || isView}
                key={key}
                id={key}
                name={key}
                error={selectBeforeFormik.errors[key]}
                options={options[getKebabCase(key)]}
                label={nameMap[getSnakeCase(key)]}
                value={getSelectValue(selectBeforeFormik, key)}
                onUpdate={(values) =>
                  selectBeforeFormik.setFieldValue(key, values[0])
                }
              />
            )
        )}

        {!isEdit && !isView && (
          <Button
            onClick={onPredict}
            disabled={isAllDisabled}
            loading={isPredictPending}
            view="action"
          >
            Оценить объявление
          </Button>
        )}

        {Object.keys(selectAfterInitialValues).map(
          (key) =>
            nameMap[getSnakeCase(key)] && (
              <Select
                disabled={isAfterActionsDisabled || isView}
                key={key}
                id={key}
                error={selectAfterFormik.errors[key]}
                name={key}
                options={options[getKebabCase(key)]}
                label={nameMap[getSnakeCase(key)]}
                value={getSelectValue(selectAfterFormik, key)}
                onUpdate={(values) =>
                  selectAfterFormik.setFieldValue(key, values[0])
                }
              />
            )
        )}

        {Object.keys(stringAfterInitialValues).map(
          (key) =>
            nameMap[getSnakeCase(key)] && (
              <TextInput
                key={key}
                error={stringAfterFormik.errors[key]}
                disabled={isAfterActionsDisabled || isView}
                id={key}
                name={key}
                type="text"
                label={nameMap[getSnakeCase(key)]}
                value={getInputValue(stringAfterFormik, key)}
                onChange={stringAfterFormik.handleChange}
              />
            )
        )}

        {Object.keys(numberAfterInitialValues).map(
          (key) =>
            nameMap[getSnakeCase(key)] &&
            !key.includes("many") && (
              <TextInput
                key={key}
                disabled={isAfterActionsDisabled || isView}
                id={key}
                name={key}
                type="number"
                error={numberAfterFormik.errors[key]}
                label={nameMap[getSnakeCase(key)]}
                value={getInputValue(numberAfterFormik, key)}
                onUpdate={(value) => {
                  numberAfterFormik.setFormikState((prev) => ({
                    ...prev,
                    values: {
                      ...prev.values,
                      [key]: value,
                    },
                  }));
                }}
              />
            )
        )}
        {isEdit && !isView && (
          <Button
            onClick={onDelete}
            disabled={isNextButtonDisabled}
            loading={isDeletePending}
            view="outlined-action"
          >
            Удалить
          </Button>
        )}
        {!isView && (
          <Button
            onClick={onNextClick}
            disabled={isNextButtonDisabled}
            loading={isAllDisabled}
            view="action"
          >
            {isEdit ? "Редактировать" : "Создать"}
          </Button>
        )}
      </Flex>
    </Dialog>
  );
};

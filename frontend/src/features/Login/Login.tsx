/* eslint-disable no-console */
import React, { useCallback, useState } from "react";

import { Button, Flex, Text, TextInput } from "@gravity-ui/uikit";
import { Link, useNavigate, useSearchParams } from "react-router-dom";

import {
  deleteCookie,
  setAdminToken,
  setUserToken,
} from "@/shared/api/getToken";
import { ADMIN_TOKEN_KEY, TOKEN_KEY } from "@/shared/constants/auth";
import { notifyErrorDefault } from "@/shared/libs/notify";

import { useLoginAdminMutation, useLoginUserMutation } from "./api";

import styles from "./Login.module.css";

export const Login = () => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [searchParams] = useSearchParams();
  const isAdminLogin = searchParams.get("admin") !== null;
  const navigate = useNavigate();
  const {
    mutate: loginUserMutation,
    isPending: isLoginUserPending,
    isError: isLoginUserError,
  } = useLoginUserMutation({
    onSuccess: (result) => {
      const token = result.data?.token;

      if (!token || typeof token !== "string") {
        notifyErrorDefault();
        return;
      }

      if (isAdminLogin) {
        setAdminToken(token);
      } else {
        setUserToken(token);
      }

      navigate("/");
    },
  });

  const {
    mutate: loginAdminMutation,
    isPending: isLoginAdminPending,
    isError: isLoginAdminError,
  } = useLoginAdminMutation({
    onSuccess: (result) => {
      const token = result.data?.token;

      if (!token || typeof token !== "string") {
        notifyErrorDefault();
        return;
      }

      if (isAdminLogin) {
        setAdminToken(token);
      } else {
        setUserToken(token);
      }

      navigate("/");
    },
  });

  const isPending = isLoginAdminPending || isLoginUserPending;
  const isError = isLoginUserError || isLoginAdminError;

  const onLoginUpdate = useCallback((updatedLogin: string) => {
    setLogin(updatedLogin);
  }, []);

  const onPasswordUpdate = useCallback((updatedPassword: string) => {
    setPassword(updatedPassword);
  }, []);

  const onCountinue = useCallback(() => {
    if (!login.length || !password.length) {
      notifyErrorDefault({ content: "Поля не должны быть пустыми" });
      return;
    }
    deleteCookie(TOKEN_KEY);
    deleteCookie(ADMIN_TOKEN_KEY);
    if (isAdminLogin) {
      loginAdminMutation({ login, password });
    } else {
      loginUserMutation({ login, password });
    }
  }, [isAdminLogin, loginAdminMutation, login, password, loginUserMutation]);

  return (
    <Flex
      direction="column"
      className={styles.root}
      justifyContent="center"
      gap={3}
      alignItems="center"
    >
      <Text variant="display-3">Логин</Text>
      <Flex direction="column" className={styles.inputs} gap={1}>
        <TextInput
          name="login"
          placeholder="Логин"
          value={login}
          onUpdate={onLoginUpdate}
          error={isError}
        />
        <TextInput
          name="password"
          placeholder="Пароль"
          type="password"
          value={password}
          onUpdate={onPasswordUpdate}
          error={isError}
        />
      </Flex>
      <Flex
        justifyContent="space-between"
        alignItems="center"
        className={styles.actions}
      >
        <Link to="/register">Еще нет аккаунта?</Link>
        <Button loading={isPending} view="action" onClick={onCountinue}>
          Продолжить
        </Button>
      </Flex>
    </Flex>
  );
};

import React from "react";

import { AdType } from "@/shared/api";
import { StateWrapperProps, withState } from "@/shared/hocs/withState";

import { AdCard } from "../AdCard";

import styles from "./AdCardList.module.css";

export interface AdCardListNormalProps {
  data: AdType[];
  onAdClick?: (id: number) => void;
}

export interface AdCardListProps extends StateWrapperProps<AdType[]> {
  onAdClick?: (id: number) => void;
}

const AdCardListLoading = () => {
  return (
    <div className={styles.root}>
      {new Array(10).fill(null).map((id, ind) => (
        <AdCard key={ind} isLoading={true} />
      ))}
    </div>
  );
};

const AdCardListNormal = (props: AdCardListNormalProps) => {
  const { data: ads, onAdClick } = props;

  return (
    <div className={styles.root}>
      {ads.map((ad, ind) => (
        <AdCard data={ad} key={ind} onAdClick={onAdClick} />
      ))}
    </div>
  );
};

const StatefulAdCardList = withState({
  Normal: AdCardListNormal,
  Loading: AdCardListLoading,
  Errored: () => <></>,
});

export const AdCardList = (props: AdCardListProps) => {
  return <StatefulAdCardList {...props} />;
};

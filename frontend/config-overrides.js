const path = require("path");

const aliases = {
  "@": path.resolve(__dirname, "src/"),
};

module.exports = (config) => {
  config.module.rules[1].oneOf.splice(0, 0, {
    test: /\.svg$/,
    use: [
      {
        loader: "@svgr/webpack",
        options: { svgo: false },
      },
    ],
  });

  config.resolve.alias = { ...config.resolve.alias, ...aliases };
  return config;
};

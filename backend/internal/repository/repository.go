package repository

import (
	"errors"
	"fmt"
	"gitlab.com/ylerby/slp/internal/database"
	"gitlab.com/ylerby/slp/internal/domain"
	"gitlab.com/ylerby/slp/internal/service/auth"
	"gitlab.com/ylerby/slp/internal/service/encryption"
)

type Repository struct {
	db   *database.Database
	auth *auth.Auth
}

func New(opts ...Option) (*Repository, error) {
	repository := new(Repository)

	errs := make([]error, 0)

	for _, o := range opts {
		if err := o(repository); err != nil {
			errs = append(errs, err)
		}
	}

	return repository, errors.Join(errs...)
}

func (r *Repository) Get() ([]domain.RealEstate, error) {
	records, err := r.db.Get()
	if err != nil {
		return nil, fmt.Errorf("records getting: %w", err)
	}

	return records, nil
}

func (r *Repository) CreateEstateRecord(fields domain.CreateRecordFields) error {
	err := r.db.Create(fields)
	if err != nil {
		return fmt.Errorf("creating estate record: %w", err)
	}

	return nil
}

func (r *Repository) AdminLogin(loginFields domain.LoginFields) (string, error) {
	user, err := r.db.AdminLogin(loginFields.Login)
	if err != nil {
		return "", fmt.Errorf("user info getting: %w", err)
	}

	if loginFields.Password != user.Password {
		return "", fmt.Errorf("invalid password")
	}

	token, err := r.auth.GenerateToken(loginFields.Login, true)
	if err != nil {
		return "", fmt.Errorf("token generating: %w", err)
	}

	return "admin:" + token, nil
}

func (r *Repository) Login(loginFields domain.LoginFields) (string, error) {
	user, err := r.db.Login(loginFields.Login)
	if err != nil {
		return "", fmt.Errorf("user info getting: %w", err)
	}

	err = encryption.CompareHashAndPassword(loginFields.Password, user.Password)
	if err != nil {
		return "", fmt.Errorf("matching: %w", err)
	}

	token, err := r.auth.GenerateToken(loginFields.Login, false)
	if err != nil {
		return "", fmt.Errorf("token generating: %w", err)
	}

	return token, nil
}

func (r *Repository) Register(registerFields domain.RegisterFields) error {
	if registerFields.Password != registerFields.AgainPassword {
		return fmt.Errorf("passwords mismatching")
	}

	hashedPassword, err := encryption.HashPassword(registerFields.Password)
	if err != nil {
		return fmt.Errorf("password encryption: %w", err)
	}

	err = r.db.Register(registerFields.Login, hashedPassword)
	if err != nil {
		return fmt.Errorf("user creating: %w", err)
	}

	return nil
}

func (r *Repository) UpdateEstateRecord(record domain.UpdateRecordFields) error {
	err := r.db.UpdateRecord(record)
	if err != nil {
		return fmt.Errorf("updating estate record: %w", err)
	}

	return nil
}

func (r *Repository) DeleteEstateRecord(id int) error {
	err := r.db.DeleteRecord(id)
	if err != nil {
		return fmt.Errorf("deleting estate record: %w", err)
	}

	return nil
}

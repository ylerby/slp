import React, { Children, ReactNode, useMemo } from "react";

import { QAProps } from "@gravity-ui/uikit";
import cx from "classnames";

import "./HorizontalFieldSet.css";

export interface HorizontalFieldSetProps extends QAProps {
  children: ReactNode;
  columns?: number[];
  tunableColumns?: string[];
  className?: string;
  isCollapsable?: boolean;
  gap?: number;
}

export const HorizontalFieldSet = (props: HorizontalFieldSetProps) => {
  const {
    children,
    columns: fractionColumns,
    tunableColumns,
    className,
    isCollapsable,
    qa,
    gap = 3,
  } = props;

  const columns = useMemo(() => {
    return (
      (fractionColumns
        ? fractionColumns.map((column) => `${column}fr`)
        : tunableColumns) ?? []
    );
  }, [fractionColumns, tunableColumns]);

  const countColumns = useMemo(() => {
    if (!isCollapsable) {
      return columns.length;
    }
    const validChildren = Children.toArray(children).filter((child) => child);

    return Math.min(validChildren.length, columns.length);
  }, [children, columns, isCollapsable]);

  const gridTemplateColumns = useMemo(() => {
    return columns.slice(0, countColumns).join(" ");
  }, [columns, countColumns]);

  return (
    <div
      className={cx(className, "HorizontalFieldSet")}
      style={{ gridTemplateColumns, gap: gap * 4 }}
      data-qa={qa}
    >
      {children}
    </div>
  );
};

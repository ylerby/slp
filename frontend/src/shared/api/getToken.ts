/* eslint-disable no-useless-escape */
import { ADMIN_TOKEN_KEY, TOKEN_KEY } from "../constants/auth";

export function deleteCookie(name: string) {
  document.cookie = name + "=; Max-Age=-99999999;";
}

function getCookie(name: string) {
  const matches = document.cookie.match(
    new RegExp(
      "(?:^|; )" +
        name.replace(/([\.$?*|{}\(\)\[\]\\\/\\+^])/g, "\\$1") +
        "=([^;]*)"
    )
  );
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name: string, value: string) {
  const options = {
    path: "/",
    "max-age": 60 * 20,
  };

  let updatedCookie =
    encodeURIComponent(name) + "=" + encodeURIComponent(value);

  for (const optionKey in options) {
    updatedCookie += "; " + optionKey;
    const optionValue = options[optionKey as keyof typeof options];
    updatedCookie += "=" + optionValue;
  }

  document.cookie = updatedCookie;
}

export function setUserToken(token: string) {
  setCookie(TOKEN_KEY, token);
}

export function setAdminToken(token: string) {
  return setCookie(ADMIN_TOKEN_KEY, token);
}

export function getUserToken() {
  return getCookie(TOKEN_KEY);
}

export function getAdminToken() {
  return getCookie(ADMIN_TOKEN_KEY);
}

export function getToken() {
  return getAdminToken() ?? getUserToken();
}

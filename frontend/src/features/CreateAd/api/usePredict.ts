import { GetPredictionPayload, HttpClient } from "@/shared/api";
import {
  UseCustomMutationOptions,
  useMutation,
} from "@/shared/hooks/useMutation";

export const usePredictMutation = (options?: UseCustomMutationOptions) => {
  const mutation = useMutation({
    mutationOptions: {
      ...options,
      mutationFn: (params: GetPredictionPayload) =>
        HttpClient.getPrediction(params),
    },
  });

  return mutation;
};

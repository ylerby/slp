package model

type Client struct {
	Login    string `gorm:"primaryKey; type:varchar(255); column:login"`
	Password string `gorm:"type:varchar(255); column:password"`
}

type Admin struct {
	Login    string `gorm:"primaryKey; type:varchar(255); column:login"`
	Password string `gorm:"type:varchar(255); column:password"`
}

package handler

import (
	"encoding/json"
	"fmt"
	"github.com/apsdehal/go-logger"
	"github.com/valyala/fasthttp"
	"gitlab.com/ylerby/slp/internal/config"
	"gitlab.com/ylerby/slp/internal/domain"
)

type Handler struct {
	r  repository
	pb publisher

	l   *logger.Logger
	cfg *config.Config
}

type repository interface {
	Get() ([]domain.RealEstate, error)
	Login(loginFields domain.LoginFields) (string, error)
	AdminLogin(loginFields domain.LoginFields) (string, error)
	Register(registerFields domain.RegisterFields) error
	UpdateEstateRecord(fields domain.UpdateRecordFields) error
	CreateEstateRecord(fields domain.CreateRecordFields) error
	DeleteEstateRecord(id int) error
}

type publisher interface {
	PublishMsg(errorText, errorSignificance string)
}

type ErrorResponse struct {
	Error string `json:"error,omitempty"`
}

func New(r repository, pb publisher, l *logger.Logger, cfg *config.Config) *Handler {
	return &Handler{
		r:  r,
		pb: pb,

		l:   l,
		cfg: cfg,
	}
}

func (h *Handler) Create(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.CreateRecordFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("unmarshaling json: %v", err.Error()), "client: minor")
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	err = h.r.CreateEstateRecord(*reqBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("creating error: %v", err.Error()), "client/service: important")
		h.l.Errorf("creating error: %v", err.Error())
		ctx.Error("creating error", fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (h *Handler) Get(ctx *fasthttp.RequestCtx) {
	records, err := h.r.Get()
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("records getting error: %v", err.Error()), "client/service: important")
		h.l.Errorf("records getting error: %v", err.Error())
		ctx.Error(fmt.Sprintf("records gettings error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	respBody, err := json.Marshal(&records)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("marshaling json: %v", err.Error()), "service: important")
		h.l.Errorf("marshaling json: %v", err.Error())
		ctx.Error("marshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(respBody)
}

func (h *Handler) Login(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.LoginFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("unmarshaling json: %v", err.Error()), "client: minor")
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	token, err := h.r.Login(*reqBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("login error: %v", err.Error()), "client/service: important")
		h.l.Errorf("login error: %v", err.Error())
		ctx.Error(fmt.Sprintf("login error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	loginResponseBody := domain.LoginResponseBody{
		Token: token,
	}

	respBody, err := json.Marshal(&loginResponseBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("marshaling json: %v", err.Error()), "service: important")
		h.l.Errorf("marshaling json: %v", err.Error())
		ctx.Error("marshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(respBody)
}

func (h *Handler) StressTest(ctx *fasthttp.RequestCtx) {
	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (h *Handler) AdminLogin(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.LoginFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("unmarshaling json: %v", err.Error()), "client: minor")
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	token, err := h.r.AdminLogin(*reqBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("login error: %v", err.Error()), "client/service: important")
		h.l.Errorf("login error: %v", err.Error())
		ctx.Error(fmt.Sprintf("login error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	loginResponseBody := domain.LoginResponseBody{
		Token: token,
	}

	respBody, err := json.Marshal(&loginResponseBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("marshaling json: %v", err.Error()), "service: important")
		h.l.Errorf("marshaling json: %v", err.Error())
		ctx.Error("marshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(respBody)
}

func (h *Handler) Register(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.RegisterFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("unmarshaling json: %v", err.Error()), "client: minor")
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	err = h.r.Register(*reqBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("register error: %v", err.Error()), "service: important")
		h.l.Errorf("register error: %v", err.Error())
		ctx.Error(fmt.Sprintf("register error: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (h *Handler) UpdateRecord(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.UpdateRecordFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("unmarshaling json: %v", err.Error()), "client: minor")
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	err = h.r.UpdateEstateRecord(*reqBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("updating error: %v", err.Error()), "client/service: important")
		h.l.Errorf("updating error: %v", err.Error())
		ctx.Error("updating error", fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (h *Handler) DeleteRecord(ctx *fasthttp.RequestCtx) {
	reqBody := new(domain.DeleteRecordFields)

	err := json.Unmarshal(ctx.Request.Body(), reqBody)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("unmarshaling json: %v", err.Error()), "client: minor")
		h.l.Errorf("unmarshaling json: %v", err.Error())
		ctx.Error("unmarshaling json error", fasthttp.StatusInternalServerError)

		return
	}

	err = h.r.DeleteEstateRecord(reqBody.ID)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("deleting error: %v", err.Error()), "client/service: important")
		h.l.Errorf("deleting error: %v", err.Error())
		ctx.Error("deleting error", fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (h *Handler) PanicHandler(ctx *fasthttp.RequestCtx, values interface{}) {
	h.l.Errorf("panic occurred: %d", fasthttp.StatusInternalServerError)
	go h.pb.PublishMsg(fmt.Sprintf("panic occurred: %v", values), "service: major")

	errorResponse := ErrorResponse{
		Error: "internal server error",
	}

	respBody, err := json.Marshal(&errorResponse)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("marshaling json: %v", err.Error()), "service: important")
		h.l.Errorf("marshaling json: %v", err.Error())
		ctx.Error(fmt.Sprintf("marshaling json: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusInternalServerError)
	ctx.SetBody(respBody)
}

func (h *Handler) MethodNotAllowedHandler(ctx *fasthttp.RequestCtx) {
	h.l.Errorf("request method now allowed: %d", fasthttp.StatusMethodNotAllowed)
	go h.pb.PublishMsg("method not allowed", "client: important")

	errorResponse := ErrorResponse{
		Error: "method now allowed",
	}

	response, err := json.Marshal(&errorResponse)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("marshaling json: %v", err.Error()), "service: important")
		h.l.Errorf("marshaling json: %v", err.Error())
		ctx.Error(fmt.Sprintf("marshaling json: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
	ctx.SetBody(response)
}

func (h *Handler) NotFoundHandler(ctx *fasthttp.RequestCtx) {
	h.l.Errorf("not found: %d", fasthttp.StatusNotFound)

	errorResponse := ErrorResponse{
		Error: "not found",
	}

	respBody, err := json.Marshal(&errorResponse)
	if err != nil {
		go h.pb.PublishMsg(fmt.Sprintf("marshaling json: %v", err.Error()), "service: important")
		h.l.Errorf("marshaling json: %v", err.Error())
		ctx.Error(fmt.Sprintf("marshaling json: %v", err.Error()), fasthttp.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusNotFound)
	ctx.SetBody(respBody)
}

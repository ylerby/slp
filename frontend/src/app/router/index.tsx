import React from "react";

import { useRoutes } from "react-router-dom";

import { appRoutes } from "./routes";

export const AppRouter = () => {
  const routes = useRoutes(appRoutes);

  return <>{routes}</>;
};

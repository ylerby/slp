import { AdType, HttpClient } from "@/shared/api";
import {
  UseCustomMutationOptions,
  useMutation,
} from "@/shared/hooks/useMutation";

export const useUpdateAdMutation = (options?: UseCustomMutationOptions) => {
  const mutation = useMutation({
    mutationOptions: {
      ...options,
      mutationFn: (params: AdType) => HttpClient.updateAd(params),
    },
    successToastOptions: {
      content: "Объявление успешно отредактировано",
    },
  });

  return mutation;
};

import React, { ComponentType } from "react";

export type ErrorType = Error | null;

export interface NullableErroredType {
  error?: ErrorType;
}

export interface ErroredType {
  error: Error;
}

export type PropsWithOmittedData<Props extends object> = Omit<Props, "data">;

export type ErroredProps<Props extends object> = ErroredType &
  PropsWithOmittedData<Props>;

export interface withStateProps<Props extends object, Data> {
  Loading: ComponentType<PropsWithOmittedData<Props>>;
  Errored: ComponentType<ErroredType & PropsWithOmittedData<Props>>;
  Normal: ComponentType<Props & { data: Data }>;
}

export interface StateWrapperProps<Data> extends NullableErroredType {
  isLoading?: boolean;
  data?: Data;
}

const StateWrapper = <Props extends object, Data>(
  wrapperProps: withStateProps<Props, Data> &
    StateWrapperProps<Data> &
    PropsWithOmittedData<Props>
) => {
  const { isLoading, data, error, Loading, Errored, Normal, ...otherProps } =
    wrapperProps;
  const propsWithoutData = otherProps as Props;

  if (error) {
    return <Errored error={error} {...propsWithoutData} />;
  }

  if (isLoading) {
    return <Loading {...propsWithoutData} />;
  }

  if (!data) {
    return (
      <Errored error={new Error("Неизвестная ошибка")} {...propsWithoutData} />
    );
  }

  return <Normal {...propsWithoutData} data={data} />;
};

export function withState<Props extends object, Data>(
  hocProps: withStateProps<Props, Data>
) {
  return (props: StateWrapperProps<Data> & PropsWithOmittedData<Props>) => {
    return <StateWrapper {...hocProps} {...props} />;
  };
}

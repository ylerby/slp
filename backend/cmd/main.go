package main

import (
	"context"
	"flag"
	slpLogger "github.com/apsdehal/go-logger"
	"github.com/valyala/fasthttp"
	"gitlab.com/ylerby/slp/internal/config"
	slpHandler "gitlab.com/ylerby/slp/internal/handler"
	slpPublisher "gitlab.com/ylerby/slp/internal/publisher"
	slpRepository "gitlab.com/ylerby/slp/internal/repository"
	slpServer "gitlab.com/ylerby/slp/internal/server"
	slpAuth "gitlab.com/ylerby/slp/internal/service/auth"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	cfgFilePath := flag.String("cfgfilepath", "configs/config.toml", "config.toml file path")
	flag.Parse()

	logger, err := slpLogger.New("slp-backend", 1, os.Stdout)
	if err != nil {
		log.Fatalf("failed to init logger: %v", err)
	}

	cfg, err := config.New(*cfgFilePath)
	if err != nil {
		logger.Fatalf("failed to init config: %v", err)
	}

	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()

	logger.Noticef("cfg: %v", cfg)

	publisher, err := slpPublisher.New(
		cfg.Nats.NatsHost,
		cfg.Nats.NatsPort,
		cfg.Nats.NatsSubject,
		logger,
	)
	if err != nil {
		logger.Fatalf("failed to init publisher: %v", err)
	}

	auth, err := slpAuth.New(
		publisher,
		cfg.Envs.JWTKey,
		cfg.Envs.AdminJWTKey,
		cfg.ApplicationCfg.JWTTTL,
	)
	if err != nil {
		logger.Fatalf("failed to init auth: %v", err)
	}

	repository, err := slpRepository.New(
		slpRepository.WithDatabase(cfg.Database),
		slpRepository.WithAuth(auth),
	)
	if err != nil {
		logger.Fatalf("failed to init repository: %v", err)
	}

	handler := slpHandler.New(
		repository,
		publisher,
		logger,
		cfg,
	)

	server := slpServer.New(
		handler,
		auth,
	)

	go func() {
		err = fasthttp.ListenAndServe(":"+cfg.Envs.ServerPort, server.Handler)
		if err != nil {
			logger.Errorf("failed to run http server: %v", err)
		}
	}()

	logger.Infof("starting API listening at :%s", cfg.Envs.ServerPort)

	<-ctx.Done()
}

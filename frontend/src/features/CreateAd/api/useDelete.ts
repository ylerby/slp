import { HttpClient } from "@/shared/api";
import {
  UseCustomMutationOptions,
  useMutation,
} from "@/shared/hooks/useMutation";

export const useDeleteAdMutation = (options?: UseCustomMutationOptions) => {
  const mutation = useMutation({
    mutationOptions: {
      ...options,
      mutationFn: (id: number) => HttpClient.deleteAd(id),
    },
    successToastOptions: {
      content: "Объявление успешно удалено",
    },
  });

  return mutation;
};

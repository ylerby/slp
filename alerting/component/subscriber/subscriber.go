package subscriber

import (
	"alerting/component/config"
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
)

func SubscribeSubject(
	msgDataCh chan []byte,
	errorCh chan error,
	natsCh chan *nats.Subscription,
	done chan struct{},
	cfg *config.Config,
) {
	nc, err := nats.Connect(fmt.Sprintf("http://%s:%s", cfg.Nats.NatsHost, cfg.Nats.NatsPort))
	if err != nil {
		errorCh <- fmt.Errorf("nats url connection: %w", err)
		return
	}
	defer nc.Close()

	sub, err := nc.Subscribe(cfg.Nats.NatsSubject, func(msg *nats.Msg) {
		msgDataCh <- msg.Data
	})
	if err != nil {
		errorCh <- fmt.Errorf("subject subscription: %w", err)
		return
	}

	natsCh <- sub

	defer func(sub *nats.Subscription) {
		err := sub.Unsubscribe()
		if err != nil {
			log.Printf("subject unsubscription: %v", err)
		}
	}(sub)

	<-done
}

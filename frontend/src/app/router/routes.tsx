import React from "react";

import { RouteObject } from "react-router-dom";

import { AdsPage } from "@/pages/AdsPage";
import { LoginPage } from "@/pages/LoginPage";
import { RegisterPage } from "@/pages/RegisterPage";

const adsRoute: RouteObject = {
  index: true,
  element: <AdsPage />,
};

const registerRoute: RouteObject = {
  path: "register",
  element: <RegisterPage />,
};

const loginRoute: RouteObject = {
  path: "login",
  element: <LoginPage />,
};

export const appRoutes = [
  {
    path: "/",
    children: [adsRoute, registerRoute, loginRoute],
  },
];

package database

import (
	"fmt"
	"gitlab.com/ylerby/slp/internal/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Database struct {
	db *gorm.DB
}

func New(dbCfg config.Database) (*Database, error) {
	db := new(Database)

	var err error

	dsn := fmt.Sprintf(
		"host=%s  user=%s password=%s  dbname=%s port=%s sslmode=disable",
		dbCfg.DatabaseHost, dbCfg.DatabaseUser, dbCfg.DatabasePassword, dbCfg.DatabaseName, dbCfg.DatabasePort,
	)

	db.db, err = gorm.Open(postgres.Open(dsn), new(gorm.Config))
	if err != nil {
		return nil, fmt.Errorf("connection: %w", err)
	}

	return db, nil
}

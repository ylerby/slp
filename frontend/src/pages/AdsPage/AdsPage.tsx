/* eslint-disable no-console */
import React, { useCallback, useMemo, useState } from "react";

import { Button, Flex, Label } from "@gravity-ui/uikit";
import { useNavigate, useSearchParams } from "react-router-dom";

import globalStyles from "@/shared/styles/global.module.css";

import { AdCardList, AdGroup } from "@/entities/Ad";
import { CreateAd } from "@/features/CreateAd";
import { FilterAds } from "@/features/FilterAds";
import { SearchAds } from "@/features/SearchAds";
import { AdType } from "@/shared/api";
import { getAdminToken, getUserToken } from "@/shared/api/getToken";
import { notifyErrorDefault } from "@/shared/libs/notify";

import { useAdsQuery } from "./api";

import styles from "./AdsPage.module.css";

export const AdsPage = () => {
  const { data: ads, error, refetch, isFetching } = useAdsQuery();
  const navigate = useNavigate();
  const [search, setSearch] = useState("");
  const [filters, setFilters] = useState<any>({});
  const [, setSearchParams] = useSearchParams();
  const isAdmin = getAdminToken();
  const isUser = getUserToken();
  const isLoggedIn = isAdmin || isUser;

  const isGroupsVisible =
    search === "" &&
    Object.values(filters.rangeValues ?? {}).every((value) => {
      const { from, to } = value as { from: number; to: number };
      return from === undefined && to === undefined;
    }) &&
    Object.keys(filters)
      .filter((key) => key !== "rangeValues")
      .every((key) => filters[key] === undefined);

  const bestOptions = useMemo(() => {
    if (!ads) {
      return [];
    }
    return ads?.slice(0, 10)?.map((ad, idx) => ({
      ...ad,
      imgSrc: `${process.env.PUBLIC_URL}/${idx + 1}.jpeg`,
    }));
  }, [ads]);

  const otherOptions = useMemo(() => {
    if (!ads) {
      return [];
    }
    return ads?.slice(10);
  }, [ads]);

  const filteredOptions = useMemo(() => {
    const filteredAds = ads?.filter((ad) => {
      const { rangeValues, ...otherFilters } = filters;
      const results = Object.entries(otherFilters).map(([key, value]) => {
        return value !== undefined
          ? ad[key as keyof AdType]
              .toString()
              .toLowerCase()
              .includes((value as string).toLowerCase())
          : true;
      });

      const result2 = Object.entries(rangeValues ?? {}).map(([key, value]) => {
        const { from, to } = value as { from: string; to: string };
        let flag = true;
        const adValue = ad[key as keyof AdType] as number;

        if (from !== undefined && adValue < parseInt(from)) {
          flag = false;
        }

        if (to !== undefined && adValue > parseInt(to)) {
          flag = false;
        }

        return flag;
      });

      const check1 = results.every((result) => result) || results.length === 0;
      const check2 = result2.every((result) => result) || result2.length === 0;

      return check1 && check2;
    });

    return filteredAds?.filter((ad) =>
      Object.values(ad).some((field) =>
        String(field).toLowerCase().includes(search.toLowerCase())
      )
    );
  }, [ads, filters, search]);

  const [isCreateDialogOpen, setIsCreateDialogOpen] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [editValues, setEditValues] = useState<AdType | undefined>();

  const onCreateClick = useCallback(() => {
    if (!isLoggedIn) {
      notifyErrorDefault({
        content:
          "Только авторизованный пользователь может создавать объявления",
      });
      return;
    }

    setIsCreateDialogOpen(true);
    setIsEdit(false);
    setEditValues(undefined);
  }, [isLoggedIn]);

  const onEditClick = useCallback(
    (id: number) => {
      const params: { view?: number } = {};
      if (!isAdmin) {
        params.view = 1;
      }

      setIsCreateDialogOpen(true);
      setIsEdit(true);
      const ad = ads?.find((ad) => ad.id === id);
      setEditValues(ad);
      setSearchParams((prev) => ({ ...prev, ...params, id }));
    },
    [ads, isAdmin, setSearchParams]
  );

  const onCreateSuccess = () => {
    refetch();
  };

  return (
    <Flex direction="column" className={globalStyles.page} gap={5}>
      <Flex gap={1} alignItems="center">
        <SearchAds search={search} onSearchUpdate={setSearch} />
        {isAdmin && (
          <Label size="m" theme="info" onClick={() => navigate("/login")}>
            Админ
          </Label>
        )}
        {!isAdmin && isUser && (
          <Label theme="info" size="m" onClick={() => navigate("/login")}>
            Пользователь
          </Label>
        )}
        {!isLoggedIn && (
          <Button
            size="m"
            view="flat-action"
            onClick={() => navigate("/login")}
          >
            Войти
          </Button>
        )}
        {!isAdmin && (
          <Button
            size="m"
            view="outlined-action"
            onClick={() => navigate("/login?admin=1")}
          >
            Войти как админ
          </Button>
        )}
        <Button size="m" view="action" onClick={onCreateClick}>
          Создать
        </Button>
      </Flex>
      <CreateAd
        open={isCreateDialogOpen}
        onClose={() => {
          setIsCreateDialogOpen(false);
          navigate("/");
        }}
        values={editValues}
        isEdit={isEdit}
        onSuccess={onCreateSuccess}
      />
      <FilterAds onSubmit={setFilters} />
      {isGroupsVisible ? (
        <>
          <AdGroup
            data={bestOptions}
            isLoading={isFetching}
            className={styles.adGroupBest}
            error={error}
            title={"Лучшие предложения"}
            onAdClick={onEditClick}
          />

          <AdGroup
            data={otherOptions}
            isLoading={isFetching}
            error={error}
            title={"Остальные предложения"}
            onAdClick={onEditClick}
          />
        </>
      ) : (
        <AdCardList
          isLoading={isFetching}
          data={filteredOptions}
          onAdClick={onEditClick}
        />
      )}
    </Flex>
  );
};

import React from "react";

import { Flex, TextInput } from "@gravity-ui/uikit";

interface SearchAdsProps {
  search: string;
  onSearchUpdate: (search: string) => void;
}

export const SearchAds = (props: SearchAdsProps) => {
  const { search, onSearchUpdate } = props;

  return (
    <TextInput
      value={search}
      onUpdate={onSearchUpdate}
      placeholder="Поиск объявления"
      size="m"
    />
  );
};

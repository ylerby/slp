package repository

import (
	"fmt"
	"gitlab.com/ylerby/slp/internal/config"
	"gitlab.com/ylerby/slp/internal/database"
	"gitlab.com/ylerby/slp/internal/service/auth"
)

type Option func(repository *Repository) error

func WithDatabase(dbCfg config.Database) Option {
	return func(r *Repository) error {
		var err error

		r.db, err = database.New(dbCfg)
		if err != nil {
			return fmt.Errorf("database init: %w", err)
		}

		return nil
	}
}

func WithAuth(auth *auth.Auth) Option {
	return func(r *Repository) error {
		r.auth = auth

		return nil
	}
}

# frontend

Запуск дев-сборки фронтенда локально:

1. git clone репозитория
2. apt install node
3. npm ci npm run start

Запуск прод-сборки фронтенда локально:

1. git clone репозитория
2. apt install node
3. npm ci
4. npm i serve -g
5. npm run build
6. serve -s build

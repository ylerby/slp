package server

import (
	"github.com/buaazp/fasthttprouter"
	"gitlab.com/ylerby/slp/internal/handler"
	"gitlab.com/ylerby/slp/internal/service/auth"
)

func New(
	handler *handler.Handler,
	auth *auth.Auth,
) *fasthttprouter.Router {
	router := &fasthttprouter.Router{
		RedirectTrailingSlash:  true,
		RedirectFixedPath:      true,
		HandleOPTIONS:          true,
		HandleMethodNotAllowed: true,

		PanicHandler:     handler.PanicHandler,
		NotFound:         handler.NotFoundHandler,
		MethodNotAllowed: handler.MethodNotAllowedHandler,
	}

	router.POST("/api/v1/login/user", handler.Login)
	router.POST("/api/v1/login/admin", handler.AdminLogin)

	router.POST("/api/v1/register", handler.Register)

	router.GET("/api/v1/get", handler.Get)

	router.POST("/api/v1/create", auth.AuthorizationMiddleware(handler.Create, false))

	router.DELETE("/api/v1/delete", auth.AuthorizationMiddleware(handler.DeleteRecord, true))

	router.PUT("/api/v1/update", auth.AuthorizationMiddleware(handler.UpdateRecord, true))

	return router
}

import { AxiosError, AxiosResponse } from "axios";

import { HttpClient, UseLoginMutationParams } from "@/shared/api";
import {
  UseCustomMutationOptions,
  useMutation,
} from "@/shared/hooks/useMutation";

export const useLoginUserMutation = (
  options?: UseCustomMutationOptions<
    AxiosResponse,
    AxiosError,
    UseLoginMutationParams,
    unknown
  >
) => {
  const mutation = useMutation({
    mutationOptions: {
      ...options,
      mutationFn: (params: UseLoginMutationParams) =>
        HttpClient.loginUser(params),
    },
  });

  return mutation;
};

export const useLoginAdminMutation = (
  options?: UseCustomMutationOptions<
    AxiosResponse,
    AxiosError,
    UseLoginMutationParams,
    unknown
  >
) => {
  const mutation = useMutation({
    mutationOptions: {
      ...options,
      mutationFn: (params: UseLoginMutationParams) =>
        HttpClient.loginAdmin(params),
    },
  });

  return mutation;
};

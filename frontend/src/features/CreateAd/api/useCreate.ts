import { HttpClient } from "@/shared/api";
import {
  UseCustomMutationOptions,
  useMutation,
} from "@/shared/hooks/useMutation";

export const useCreateAdMutation = (options?: UseCustomMutationOptions) => {
  const mutation = useMutation({
    mutationOptions: {
      ...options,
      mutationFn: (params: any) => HttpClient.createAd(params),
    },
    successToastOptions: {
      content: "Объявление успешно создано",
    },
  });

  return mutation;
};

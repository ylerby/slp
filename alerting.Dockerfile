FROM golang:1.21.4

RUN go version
ENV GOPATH=/

WORKDIR /alerting

COPY /alerting .

RUN apt-get update

WORKDIR /alerting
RUN go mod download
RUN go build -o alert ./cmd/main.go

CMD ["/alerting/alert"]
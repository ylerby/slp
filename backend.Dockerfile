FROM golang:1.21.4

RUN go version
ENV GOPATH=/

WORKDIR /backend

COPY /backend .

RUN apt-get update

WORKDIR /backend
RUN go mod download
RUN go build -o api ./cmd/main.go

CMD ["/backend/api"]
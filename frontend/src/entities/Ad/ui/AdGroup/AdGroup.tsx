import React from "react";

import { Flex, Text } from "@gravity-ui/uikit";

import { AdType } from "@/shared/api";
import { StateWrapperProps, withState } from "@/shared/hocs/withState";

import { AdCardList } from "../AdCardList";

export interface AdGroupLoadingProps {
  title: string;
  className?: string;
  onAdClick: (id: number) => void;
}

export interface AdGroupNormalProps extends AdGroupLoadingProps {
  data: AdType[];
}

export type AdGroupProps = StateWrapperProps<AdType[]> & AdGroupLoadingProps;

export const AdGroupLoading = (props: AdGroupLoadingProps) => {
  const { title, className } = props;

  return (
    <Flex direction="column" gap={3} className={className}>
      <Text variant="header-1">{title}</Text>
      <AdCardList isLoading />
    </Flex>
  );
};

export const AdGroupNormal = (props: AdGroupNormalProps) => {
  const { data: ads, title, className, onAdClick } = props;
  return (
    <Flex direction="column" gap={3} className={className}>
      <Text variant="header-1">{title}</Text>
      <AdCardList data={ads} onAdClick={onAdClick} />
    </Flex>
  );
};

const StatefulAdGroup = withState({
  Normal: AdGroupNormal,
  Loading: AdGroupLoading,
  Errored: () => <></>,
});

export const AdGroup = (props: AdGroupProps) => {
  return <StatefulAdGroup {...props} />;
};

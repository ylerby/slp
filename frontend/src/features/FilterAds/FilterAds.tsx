/* eslint-disable no-console */
import React, { useMemo } from "react";

import {
  Button,
  Flex,
  Select,
  SelectOption,
  Text,
  TextInput,
} from "@gravity-ui/uikit";
import { useFormik } from "formik";
import * as Yup from "yup";

import {
  AdType,
  ApartmentType,
  Building,
  ObjectType,
  Region,
} from "@/shared/api";
import { stations } from "@/shared/constants/stations";
import { HorizontalFieldSet } from "@/shared/ui";

export type Filters = Partial<{
  priceFrom: AdType["price"];
  priceTo: AdType["price"];
  levelFrom: AdType["level"];
  levelTo: AdType["level"];
  levelsFrom: AdType["levels"];
  levelsTo: AdType["levels"];
  roomsFrom: AdType["rooms"];
  roomsTo: AdType["rooms"];
  areaFrom: AdType["area"];
  areaTo: AdType["area"];
  "kitchen-areaFrom": AdType["kitchen-area"];
  "kitchen-areaTo": AdType["kitchen-area"];
  region: AdType["region"];
  "house-number": AdType["house-number"];
  street: AdType["street"];
  "object-type": AdType["object-type"];
  "apartment-type": AdType["apartment-type"];
  "nearest-metro-station": AdType["nearest-metro-station"];
  city: AdType["city"];
  "building-type": AdType["building-type"];
}>;

interface FilterAdsProps {
  onSubmit: (filters: any) => void;
}

const initialValues: Filters = {
  priceFrom: undefined,
  priceTo: undefined,
  levelFrom: undefined,
  levelTo: undefined,
  levelsFrom: undefined,
  levelsTo: undefined,
  roomsFrom: undefined,
  roomsTo: undefined,
  areaFrom: undefined,
  areaTo: undefined,
  "kitchen-areaFrom": undefined,
  "kitchen-areaTo": undefined,
  region: undefined,
  "house-number": undefined,
  street: undefined,
  "object-type": undefined,
  "apartment-type": undefined,
  "nearest-metro-station": undefined,
  city: undefined,
  "building-type": undefined,
};

export function getSelectOptions(
  options: Record<string, string> | string[]
): SelectOption[] {
  return Object.values(options).map((option) => ({
    content: option,
    value: option,
  }));
}

export const buildingOptions = getSelectOptions(Building);
export const regionOptions = getSelectOptions(Region);
export const apartmentTypeOptions = getSelectOptions(ApartmentType);
export const metroOptions = getSelectOptions(stations as unknown as string[]);
export const objectTypeOptions = getSelectOptions(ObjectType);

export const FilterAds = (props: FilterAdsProps) => {
  const { onSubmit } = props;
  const validationSchema = useMemo(() => {
    return Yup.object({
      "house-number": Yup.number().positive("Номер дома должен не меньше 0"),
      areaFrom: Yup.number().min(20, "Площадь должна быть не меньше 20"),
      areaTo: Yup.number().min(20, "Площадь должна быть не меньше 20"),
      "kitchen-areaTo": Yup.number().min(
        0,
        "Площадь кухни должна быть не меньше 0"
      ),
      "kitchen-areaFrom": Yup.number().min(
        0,
        "Площадь кухни должна быть не меньше 0"
      ),
      roomsTo: Yup.number().min(1, "Количество комнат должно быть не меньше 1"),
      roomsFrom: Yup.number().min(
        1,
        "Количество комнат должно быть не меньше 1"
      ),
      priceTo: Yup.number().min(1000000, "Цена должна быть не меньше 1000000"),
      priceFrom: Yup.number().min(
        1000000,
        "Цена должна быть не меньше 1000000"
      ),

      levelTo: Yup.number().positive("Этаж должен быть не меньше 1"),
      levelFrom: Yup.number().positive("Этаж должен быть не меньше 1"),
      levelsTo: Yup.number().min(
        1,
        "Количество этажей должно быть не меньше 1"
      ),
      // .when("level", ([level], schema) =>
      //   level !== undefined
      //     ? schema.min(
      //         Yup.ref("level"),
      //         "Количество этажей в доме должно быть не меньше этажа"
      //       )
      //     : schema
      // ),
      levelsFrom: Yup.number().min(
        1,
        "Количество этажей должно быть не меньше 1"
      ),
      // .when("level", ([level], schema) =>
      //   level !== undefined
      //     ? schema.min(
      //         Yup.ref("level"),
      //         "Количество этажей в доме должно быть не меньше этажа"
      //       )
      //     : schema
      // ),
      //   region: Yup.string().matches(
      //     /^[А-Я][а-я]*$/,
      //     "Регион должен быть на кириллице и с большой буквы"
      //   ),
    });
  }, []);

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values) => {
      const {
        priceTo,
        priceFrom,
        areaFrom,
        areaTo,
        "kitchen-areaFrom": kitchenAreaFrom,
        "kitchen-areaTo": kitchenAreaTo,
        levelFrom,
        levelTo,
        levelsFrom,
        levelsTo,
        roomsFrom,
        roomsTo,
        ...otherValues
        // region,
        // "house-number": houseNumber,
        // street,
        // "object-type": objectType,
        // "apartment-type": apartmentType,
        // "nearest-metro-station": metro,
        // city,
        // "building-type": buildingType,
      } = values;

      const rangeValues = {
        price: { to: priceTo, from: priceFrom },
        area: { to: areaFrom, from: areaTo },
        "kitchen-area": { to: kitchenAreaTo, from: kitchenAreaFrom },
        level: { to: levelTo, from: levelFrom },
        levels: { to: levelsTo, from: levelsFrom },
        rooms: { to: roomsTo, from: roomsFrom },
      };

      onSubmit({ rangeValues, ...otherValues });
    },
  });

  const arr = [
    {
      title: "Цена",
      name: "price",
    },
    {
      title: "Количество комнат",
      name: "rooms",
    },
    {
      title: "Этаж",
      name: "level",
    },
    {
      title: "Этажей в доме",
      name: "levels",
    },
    {
      title: "Площадь",
      name: "area",
    },
    {
      title: "Площадь кухни",
      name: "kitchen-area",
    },
  ] as const;

  const arr1 = [
    {
      title: "Город",
      name: "city",
    },
    {
      title: "Улица",
      name: "street",
    },
    {
      title: "Номер дома",
      name: "house-number",
    },
  ] as const;

  const arr2 = [
    {
      title: "Регион",
      name: "region",
      options: regionOptions,
    },
    {
      title: "Тип строения",
      name: "building-type",
      options: buildingOptions,
    },
    {
      title: "Метро",
      name: "nearest-metro-station",
      options: metroOptions,
    },
    {
      title: "Тип квартиры",
      name: "apartment-type",
      options: apartmentTypeOptions,
    },
    {
      title: "Тип объекта",
      name: "object-type",
      options: objectTypeOptions,
    },
  ] as const;

  return (
    <Flex direction="column" gap={2}>
      <Flex gap={1} justifyContent="space-between" wrap="wrap">
        <Flex gap={1} direction="column">
          {arr.map(({ title, name }) => (
            <HorizontalFieldSet key={title} tunableColumns={["200px", "400px"]}>
              <Text variant="subheader-3">{title}</Text>
              <Flex gap={1}>
                <TextInput
                  type="number"
                  name={`${name}From`}
                  placeholder="От"
                  onUpdate={(value) =>
                    formik.setFieldValue(
                      `${name}From`,
                      value !== "" ? value : undefined
                    )
                  }
                  onChange={formik.handleChange}
                  error={formik.errors[`${name}From`]}
                />
                <TextInput
                  type="number"
                  name={`${name}To`}
                  placeholder="До"
                  onUpdate={(value) =>
                    formik.setFieldValue(
                      `${name}To`,
                      value !== "" ? value : undefined
                    )
                  }
                  error={formik.errors[`${name}To`]}
                />
              </Flex>
            </HorizontalFieldSet>
          ))}
        </Flex>

        <Flex gap={1} direction="column">
          {arr1.map(({ title, name }) => (
            <HorizontalFieldSet key={title} tunableColumns={["200px", "400px"]}>
              <Text variant="subheader-3">{title}</Text>
              <TextInput
                key={title}
                type="text"
                name={name}
                placeholder={title}
                onUpdate={(value) =>
                  formik.setFieldValue(name, value !== "" ? value : undefined)
                }
              />
            </HorizontalFieldSet>
          ))}
        </Flex>
      </Flex>

      {arr2.map(({ title, name, options }) => (
        <HorizontalFieldSet key={title} tunableColumns={["200px", "400px"]}>
          <Text variant="subheader-3">{title}</Text>
          <Select
            hasClear
            key={title}
            width="auto"
            options={options}
            label={title}
            name={name}
            onUpdate={(values) => formik.setFieldValue(name, values[0])}
          />
        </HorizontalFieldSet>
      ))}

      {/* <Checkbox name="Много культурных объектов" />
      <Checkbox name="Много комфортных объектов" />
      <Checkbox name="Много транспорта" />
      <Checkbox name="Много отелей" />
      <Checkbox name="Много исторических объектов" />
      <Checkbox name="Много мест для развлекательния" />
      <Checkbox name="Много финансовых организаций" />
      <Checkbox name="Много магазинов" />
      <Checkbox name="Много ресторанов" />
      <Checkbox name="Много офисов" /> */}
      <Flex justifyContent="flex-end">
        <Button view="action" onClick={() => formik.handleSubmit()}>
          Применить фильтры
        </Button>
      </Flex>
    </Flex>
  );
};

package auth

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/valyala/fasthttp"
	"log"
	"strings"
	"time"
)

type publisher interface {
	PublishMsg(errorText, errorSignificance string)
}

type ErrorResponse struct {
	Error string `json:"error,omitempty"`
}

type Auth struct {
	pb publisher

	jwtKey      []byte
	adminJwtKey []byte
	jwtttl      int
}

func New(pb publisher, jwtKey string, adminJwtKey string, JWTTTL int) (*Auth, error) {
	if jwtKey == "" {
		return nil, fmt.Errorf("empty jwt secret key")
	}

	if JWTTTL <= 0 {
		return nil, fmt.Errorf("invalid jwt ttl")
	}

	return &Auth{
		pb:          pb,
		jwtttl:      JWTTTL,
		jwtKey:      []byte(jwtKey),
		adminJwtKey: []byte(adminJwtKey),
	}, nil
}

type Claims struct {
	Login string `json:"login"`
	jwt.StandardClaims
}

func (a *Auth) GenerateToken(username string, isAdmin bool) (string, error) {
	expirationTime := time.Now().Add(time.Duration(a.jwtttl) * time.Minute)
	claims := &Claims{
		Login: username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	var (
		tokenString string
		err         error
	)

	if isAdmin {
		tokenString, err = token.SignedString(a.adminJwtKey)
	} else {
		tokenString, err = token.SignedString(a.jwtKey)
	}

	if err != nil {
		return "", fmt.Errorf("token generation: %w", err)
	}

	return tokenString, nil
}

func (a *Auth) AuthorizationMiddleware(next fasthttp.RequestHandler, adminRequest bool) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		authHeader := string(ctx.Request.Header.Peek("Authorization"))
		if authHeader == "" {
			ctx.Error("missing auth header", fasthttp.StatusUnauthorized)
			return
		}

		tokenString := authHeader[len("Bearer "):]

		var (
			isAdmin bool
			err     error
			token   *jwt.Token
		)

		claims := new(Claims)

		if tokenWithoutAdminPrefix, found := strings.CutPrefix(tokenString, "admin:"); found {
			isAdmin = true
			token, err = jwt.ParseWithClaims(tokenWithoutAdminPrefix, claims, func(token *jwt.Token) (interface{}, error) {
				return a.adminJwtKey, nil
			})
		} else {
			token, err = jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
				return a.jwtKey, nil
			})
		}

		if adminRequest && !isAdmin {
			ctx.Error("bad request", fasthttp.StatusBadRequest)
			return
		}

		if err != nil || !token.Valid {
			log.Printf("auth error: %v\n", err)
			ctx.Error("invalid auth header", fasthttp.StatusUnauthorized)
			return
		}
		next(ctx)
	}
}
